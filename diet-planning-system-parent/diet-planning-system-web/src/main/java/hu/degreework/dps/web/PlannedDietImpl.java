package hu.degreework.dps.web;

import java.util.List;

import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RestController;

import hu.degreework.dps.api.PlannedDiet;
import hu.degreework.dps.api.PlannedDietApi;
import hu.degreework.dps.service.PlannedDietService;

@RestController
public class PlannedDietImpl implements PlannedDietApi{

	private final PlannedDietService service;
	
	public PlannedDietImpl(PlannedDietService service) {
		this.service = service;
	}

	@Override
	public List<PlannedDiet> getAll(Authentication anAuthentication) {
		return service.getAll(anAuthentication.getName());
	}

	@Override
	public void delete(String plannedDietId, Authentication anAuthentication) {
		service.delete(plannedDietId, anAuthentication.getName());
	}
	
}
