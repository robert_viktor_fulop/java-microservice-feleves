package hu.degreework.dps.web;

import java.util.List;
import java.util.Set;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import hu.degreework.dps.api.Recipe;
import hu.degreework.dps.api.RecipeApi;
import hu.degreework.dps.service.RecipeService;

@RestController
public class RecipeImpl implements RecipeApi {

	private final RecipeService service;

	public RecipeImpl(RecipeService service) {
		this.service = service;
	}
	
	@Override
	public ResponseEntity<Set<Recipe>> getAll(Integer pageNo, Integer pageSize) {
		 Set<Recipe> list = service.getAll(pageNo, pageSize);
	     return new ResponseEntity<Set<Recipe>>(list, new HttpHeaders(), HttpStatus.OK); 
	}


	@Override
	public Set<Recipe> getAllWithoutPages() {
		
		return service.getAll();
	}
	
	
	@Override
	public Recipe getById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

}
