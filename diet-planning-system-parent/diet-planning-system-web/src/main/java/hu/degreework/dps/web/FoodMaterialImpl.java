package hu.degreework.dps.web;

import java.util.Set;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import hu.degreework.dps.api.FoodMaterial;
import hu.degreework.dps.api.FoodMaterialApi;
import hu.degreework.dps.service.FoodMaterialService;
import hu.degreework.dps.service.exception.FoodMaterialNotFound;

@RestController
public class FoodMaterialImpl implements FoodMaterialApi{
	
	private final FoodMaterialService service;
	
	public FoodMaterialImpl(FoodMaterialService service) {
		this.service = service;
	}
	
	@Override
	public Set<FoodMaterial> getAll() {
		return service.getAll();
	}

	@Override
	public FoodMaterial getById(String id) {
		try {
			return service.getById(id);
		} catch (FoodMaterialNotFound e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
	}

}
