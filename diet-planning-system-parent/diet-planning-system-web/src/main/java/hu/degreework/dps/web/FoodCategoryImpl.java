package hu.degreework.dps.web;

import java.util.List;
import java.util.Set;

import org.springframework.web.bind.annotation.RestController;

import hu.degreework.dps.api.FoodCategory;
import hu.degreework.dps.api.FoodCategoryApi;
import hu.degreework.dps.service.FoodCategoryService;

@RestController
public class FoodCategoryImpl implements FoodCategoryApi{

	private final FoodCategoryService service;
	
	public FoodCategoryImpl(FoodCategoryService service) {
		this.service = service;
	}
	
	public Set<FoodCategory> getAll() {
		return service.getAll();
	}

	public FoodCategory addFoodCategory(FoodCategory foodCategory) {
		return service.create(foodCategory);
	}

	public List<FoodCategory> addAllFoodCategory(List<FoodCategory> foodCategories) {
		return service.createAll(foodCategories);
	}

	public FoodCategory getFoodCategoryById(String id) {
		return service.findById(id);
	}

	public void deleteFoodCategory(String id) {
		service.deleteById(id);
	}
}
