package hu.degreework.dps.web;

import java.util.List;
import java.util.Set;

import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RestController;

import hu.degreework.dps.api.DietCalendar;
import hu.degreework.dps.api.DietCalendarApi;
import hu.degreework.dps.service.DietCalendarService;

@RestController
public class DietCalendarImpl implements DietCalendarApi{

	private final DietCalendarService service;
	
	public DietCalendarImpl(DietCalendarService service) {
		this.service = service;
	}
	
	@Override
	public void save(List<DietCalendar> dietCalendar, Authentication anAuthentication) {
		service.save(dietCalendar, anAuthentication.getName());	
	}

	@Override
	public List<DietCalendar> getAll(Authentication anAuthentication) {
		return service.getAll(anAuthentication.getName());
	}

}
