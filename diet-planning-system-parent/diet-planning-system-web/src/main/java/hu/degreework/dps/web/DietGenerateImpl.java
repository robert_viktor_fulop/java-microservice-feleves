package hu.degreework.dps.web;

import java.util.List;
import java.util.Set;

import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RestController;

import hu.degreework.dps.api.DietGenerate;
import hu.degreework.dps.api.DietGenerateApi;
import hu.degreework.dps.api.GeneratedDiets;
import hu.degreework.dps.service.DietGenerateService;

@RestController
public class DietGenerateImpl implements DietGenerateApi {
	
	private final DietGenerateService service;
	
	public DietGenerateImpl(DietGenerateService service) {
		this.service = service;
	}
	
	@Override
	public List<GeneratedDiets> generateStart(DietGenerate dietGenerate, Authentication anAuthentication) {
		return service.dietGeneration(dietGenerate, anAuthentication.getName());	
	}

	@Override
	public void saveGeneratedDiets(List<GeneratedDiets> generatedDiets,
			Authentication anAuthentication) {
		service.save(generatedDiets, anAuthentication.getName());
	}

}
