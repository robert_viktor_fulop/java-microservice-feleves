package hu.degreework.dps.web;

import java.util.Set;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import hu.degreework.dps.api.Gender;
import hu.degreework.dps.api.GenderApi;
import hu.degreework.dps.service.GenderService;
import hu.degreework.dps.service.exception.GenderNotFound;

@RestController
public class GenderImpl implements GenderApi {

	private final GenderService service;

	public GenderImpl(GenderService service) {
		this.service = service;
	}

	@Override
	public Gender getById(String id) {
		try {
			return service.getById(id);
		} catch (GenderNotFound e) {
			throw new ResponseStatusException(HttpStatus.NO_CONTENT);
		}
	}

	@Override
	public Set<Gender> getAll() {
		return service.getAll();
	}

}
