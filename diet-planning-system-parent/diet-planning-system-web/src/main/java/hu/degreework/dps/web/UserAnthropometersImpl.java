package hu.degreework.dps.web;

import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RestController;
import hu.degreework.dps.api.UserAnthropometers;
import hu.degreework.dps.api.UserAnthropometersApi;
import hu.degreework.dps.service.UserAnthropometersService;

@RestController
public class UserAnthropometersImpl implements UserAnthropometersApi {
	private final UserAnthropometersService service;

	public UserAnthropometersImpl(UserAnthropometersService service) {
		this.service = service;
	}

	@Override
	public UserAnthropometers add(UserAnthropometers fiber, Authentication anAuthentication) {
		return service.create(fiber, anAuthentication.getName());

	}

	@Override
	public UserAnthropometers getAnthropometers(Authentication anAuthentication) {
		String keycloakId = anAuthentication.getName();
		return service.findByUserId(keycloakId);
	}

	@Override
	public UserAnthropometers update(UserAnthropometers fiber, Authentication anAuthentication) {
		// TODO Auto-generated method stub
		return null;
	}

}
