package hu.degreework.dps.web;

import org.springframework.web.bind.annotation.RestController;

import hu.degreework.dps.api.Anthropometers;
import hu.degreework.dps.api.AnthropometersApi;
import hu.degreework.dps.api.BodyMassIndex;
import hu.degreework.dps.api.BodyFatPercentage;
import hu.degreework.dps.api.EnergyDemand;
import hu.degreework.dps.service.CalculatorsService;

@RestController
public class CalculatorsImpl implements AnthropometersApi {

	private final CalculatorsService service;

	public CalculatorsImpl(CalculatorsService service) {
		this.service = service;
	}

	public BodyMassIndex bmiCalculation(Anthropometers anthropometers) {
		BodyMassIndex bodyMassIndex = new BodyMassIndex();
		return service.bmiCalculation(anthropometers, bodyMassIndex);
	}

	public EnergyDemand energyDemandCalculation(Anthropometers anthropometers) {
		EnergyDemand energyDemand = new EnergyDemand();
		
		switch (anthropometers.getGender()) {
		case FEMALE:
			return service.energyDemandCalculationFemale(anthropometers, energyDemand);
			
		case MALE:
			return service.energyDemandCalculationMale(anthropometers, energyDemand);
			
		default:
			return energyDemand;
		}
	}

	public BodyFatPercentage bodyFatPercentageCalculation(Anthropometers anthropometers) {
		BodyFatPercentage bodyFatPercentag = new BodyFatPercentage();
		
		switch (anthropometers.getGender()) {
		case FEMALE:
			return service.bodyFatPercentageFemale(anthropometers, bodyFatPercentag);
			
		case MALE:
			return service.bodyFatPercentageMale(anthropometers, bodyFatPercentag);
			
		default:
			return bodyFatPercentag;
		}
	}

}
