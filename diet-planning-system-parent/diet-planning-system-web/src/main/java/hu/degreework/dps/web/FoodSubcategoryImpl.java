package hu.degreework.dps.web;

import java.util.List;
import java.util.Set;
import org.springframework.web.bind.annotation.RestController;

import hu.degreework.dps.api.FoodSubcategory;
import hu.degreework.dps.api.FoodSubcategoryApi;
import hu.degreework.dps.service.FoodSubcateroyService;

@RestController
public class FoodSubcategoryImpl implements FoodSubcategoryApi{

	private final FoodSubcateroyService service;
	
	public FoodSubcategoryImpl(FoodSubcateroyService service) {
		this.service = service;
	}
	
	public Set<FoodSubcategory> getAll() {
		return service.getAll();
	}

	public FoodSubcategory addFoodSubcategory(FoodSubcategory foodSubcategory) {
		return service.create(foodSubcategory);
	}

	public List<FoodSubcategory> addAllFoodSubcategory(List<FoodSubcategory> foodSubcategories) {
		return service.createAll(foodSubcategories);
	}

	public FoodSubcategory getFoodSubcategoryById(String id) {
		return service.findById(id);
	}

	public void deleteFoodSubcategory(String id) {
		service.deleteById(id);
	}

}
