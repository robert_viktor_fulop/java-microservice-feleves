package hu.degreework.dps.api;

import java.util.Objects;

public class Gender {

	private String id;
	private GenderType genderType;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public GenderType getGenderType() {
		return genderType;
	}

	public void setGenderType(GenderType genderType) {
		this.genderType = genderType;
	}

	@Override
	public int hashCode() {
		return Objects.hash(genderType, id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Gender other = (Gender) obj;
		return genderType == other.genderType && Objects.equals(id, other.id);
	}

}
