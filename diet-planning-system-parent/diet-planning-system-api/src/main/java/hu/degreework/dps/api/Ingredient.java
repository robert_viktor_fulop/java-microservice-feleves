package hu.degreework.dps.api;

import java.math.BigDecimal;
import java.util.Objects;

public class Ingredient {

	private String id;
	private BigDecimal quantity;
	private String unitOfMeasures;
	private String foodMaterialName;
	private String recipeId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public String getUnitOfMeasures() {
		return unitOfMeasures;
	}

	public void setUnitOfMeasures(String unitOfMeasures) {
		this.unitOfMeasures = unitOfMeasures;
	}

	public String getFoodMaterialName() {
		return foodMaterialName;
	}

	public void setFoodMaterialName(String foodMaterialName) {
		this.foodMaterialName = foodMaterialName;
	}

	public String getRecipeId() {
		return recipeId;
	}

	public void setRecipeId(String recipeId) {
		this.recipeId = recipeId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(foodMaterialName, id, quantity, recipeId, unitOfMeasures);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ingredient other = (Ingredient) obj;
		return Objects.equals(foodMaterialName, other.foodMaterialName) && Objects.equals(id, other.id)
				&& Objects.equals(quantity, other.quantity) && Objects.equals(recipeId, other.recipeId)
				&& Objects.equals(unitOfMeasures, other.unitOfMeasures);
	}
}
