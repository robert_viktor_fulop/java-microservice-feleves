package hu.degreework.dps.api;

import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum OilPreferencesType {
	NONE(0),
	NO_SELECTION(1),
	AVOCADE(2),
	OLIVE(3),
	WALNUT(4),
	RAPESEED(5),
	FLAX(6);
	
	private Integer code;
	
	private OilPreferencesType(Integer code) {
		this.code = code;
	}
	
	@JsonCreator
	public static OilPreferencesType decode(final Integer code) {
		return Stream.of(OilPreferencesType.values()).filter(targetEnum -> targetEnum.code.equals(code)).findFirst().orElse(null);
	}
	
	@JsonValue
	public Integer getCode() {
		return code;
	}
}
