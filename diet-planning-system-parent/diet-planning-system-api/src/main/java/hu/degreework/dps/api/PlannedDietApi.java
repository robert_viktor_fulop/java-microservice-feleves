package hu.degreework.dps.api;

import java.util.List;
import java.util.Set;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RequestMapping(value = "/api")
@Tag(name = "PlannedDiet", description = "The PlannedDiet API")
public interface PlannedDietApi {

	@Operation(summary = "Find all PlannedDiet", description = "Returns all PlannedDiet", tags = {"PlannedDiet"})
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
    @GetMapping(value = "/planned-diets", produces = MediaType.APPLICATION_JSON_VALUE)
	List<PlannedDiet> getAll(Authentication anAuthentication);
		
    @Operation(summary = "Delete PlannedDiet", description = "", tags = { "PlannedDiet" })
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation.") })
    @PreAuthorize("hasRole('REGISTERED_USER')")
    @DeleteMapping(value = "/planned-diet/{id}")
    void delete(@PathVariable(value = "id") String id, Authentication anAuthentication);
}
