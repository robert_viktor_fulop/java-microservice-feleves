package hu.degreework.dps.api;

import java.util.List;
import java.util.Objects;
import java.util.Set;

public class PlannedDiet {

	private String id;
	private List<GeneratedDiets> diets;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<GeneratedDiets> getDiets() {
		return diets;
	}

	public void setDiets(List<GeneratedDiets> diets) {
		this.diets = diets;
	}

	@Override
	public int hashCode() {
		return Objects.hash(diets, id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlannedDiet other = (PlannedDiet) obj;
		return Objects.equals(diets, other.diets) && Objects.equals(id, other.id);
	}

}
