package hu.degreework.dps.api;

import java.util.Set;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RequestMapping(value = "/api")
@Tag(name = "Mineral Statistics", description = "The Mineral Statistics API")
public interface MineralStatisticsApi {

	@Operation(summary = "Find all Mineral Statistics", description = "Returns all Mineral Statistics", tags = { "MineralStatistics" })
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
    @GetMapping(value = "/mineral-statistics", produces = MediaType.APPLICATION_JSON_VALUE)
    Set<UserDiet> getAll(Authentication anAuthentication);
			
	@Operation(summary = "Find User Diet by id", description = "Returns Mineral Statistics by the given id", tags = {
    "MineralStatistics" })
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation."),
    @ApiResponse(responseCode = "404", description = "MineralStatistics not found.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
	@GetMapping(value = "/mineral-statistics/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	UserDiet getById(@PathVariable("id") String id, Authentication anAuthentication);
	
}
