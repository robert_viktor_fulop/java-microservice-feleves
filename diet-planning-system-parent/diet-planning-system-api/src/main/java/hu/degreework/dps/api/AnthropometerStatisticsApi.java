package hu.degreework.dps.api;

import java.util.Set;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RequestMapping(value = "/api")
@Tag(name = "Anthropometer Statistics", description = "The Anthropometer Statistics API")
public interface AnthropometerStatisticsApi {

	@Operation(summary = "Find all Anthropometer statistics", description = "Returns all Anthropometer Statistics", tags = { "AnthropometerStatistics" })
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
    @GetMapping(value = "/anthropometer-statistics", produces = MediaType.APPLICATION_JSON_VALUE)
    Set<AnthropometerStatistics> getAll(Authentication anAuthentication);
			
	@Operation(summary = "Find Anthropometer Statistics by id", description = "Returns Anthropometer Statistics by the given id", tags = {
    "AnthropometerStatistics" })
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation."),
    @ApiResponse(responseCode = "404", description = "AnthropometerStatistics not found.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
	@GetMapping(value = "/anthropometer-statistics/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	AnthropometerStatistics getById(@PathVariable("id") String id, Authentication anAuthentication);
}
