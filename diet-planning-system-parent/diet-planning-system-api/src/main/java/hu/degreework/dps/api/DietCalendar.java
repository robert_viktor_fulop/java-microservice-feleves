package hu.degreework.dps.api;

import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;

public class DietCalendar {

	private String id;
	private String plannedDietId;
	private String dailyDietId;
	private String breakfastId;
	private String breakfastName;
	private String brunchId;
	private String brunchName;
	private String lunchSoupId;
	private String lunchSoupName;
	private String lunchMainDishId;
	private String lunchMainDishName;
	private String snackId;
	private String snackName;
	private String dinnerId;
	private String dinnerName;
	private LocalDate startDate;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPlannedDietId() {
		return plannedDietId;
	}

	public void setPlannedDietId(String plannedDietId) {
		this.plannedDietId = plannedDietId;
	}

	public String getDailyDietId() {
		return dailyDietId;
	}

	public void setDailyDietId(String dailyDietId) {
		this.dailyDietId = dailyDietId;
	}

	public String getBreakfastId() {
		return breakfastId;
	}

	public void setBreakfastId(String breakfastId) {
		this.breakfastId = breakfastId;
	}

	public String getBreakfastName() {
		return breakfastName;
	}

	public void setBreakfastName(String breakfastName) {
		this.breakfastName = breakfastName;
	}

	public String getBrunchId() {
		return brunchId;
	}

	public void setBrunchId(String brunchId) {
		this.brunchId = brunchId;
	}

	public String getBrunchName() {
		return brunchName;
	}

	public void setBrunchName(String brunchName) {
		this.brunchName = brunchName;
	}

	public String getLunchSoupId() {
		return lunchSoupId;
	}

	public void setLunchSoupId(String lunchSoupId) {
		this.lunchSoupId = lunchSoupId;
	}

	public String getLunchSoupName() {
		return lunchSoupName;
	}

	public void setLunchSoupName(String lunchSoupName) {
		this.lunchSoupName = lunchSoupName;
	}

	public String getLunchMainDishId() {
		return lunchMainDishId;
	}

	public void setLunchMainDishId(String lunchMainDishId) {
		this.lunchMainDishId = lunchMainDishId;
	}

	public String getLunchMainDishName() {
		return lunchMainDishName;
	}

	public void setLunchMainDishName(String lunchMainDishName) {
		this.lunchMainDishName = lunchMainDishName;
	}

	public String getSnackId() {
		return snackId;
	}

	public void setSnackId(String snackId) {
		this.snackId = snackId;
	}

	public String getSnackName() {
		return snackName;
	}

	public void setSnackName(String snackName) {
		this.snackName = snackName;
	}

	public String getDinnerId() {
		return dinnerId;
	}

	public void setDinnerId(String dinnerId) {
		this.dinnerId = dinnerId;
	}

	public String getDinnerName() {
		return dinnerName;
	}

	public void setDinnerName(String dinnerName) {
		this.dinnerName = dinnerName;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	@Override
	public int hashCode() {
		return Objects.hash(breakfastId, breakfastName, brunchId, brunchName, dailyDietId, dinnerId, dinnerName, id,
				lunchMainDishId, lunchMainDishName, lunchSoupId, lunchSoupName, plannedDietId, snackId, snackName,
				startDate);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DietCalendar other = (DietCalendar) obj;
		return Objects.equals(breakfastId, other.breakfastId) && Objects.equals(breakfastName, other.breakfastName)
				&& Objects.equals(brunchId, other.brunchId) && Objects.equals(brunchName, other.brunchName)
				&& Objects.equals(dailyDietId, other.dailyDietId) && Objects.equals(dinnerId, other.dinnerId)
				&& Objects.equals(dinnerName, other.dinnerName) && Objects.equals(id, other.id)
				&& Objects.equals(lunchMainDishId, other.lunchMainDishId)
				&& Objects.equals(lunchMainDishName, other.lunchMainDishName)
				&& Objects.equals(lunchSoupId, other.lunchSoupId) && Objects.equals(lunchSoupName, other.lunchSoupName)
				&& Objects.equals(plannedDietId, other.plannedDietId) && Objects.equals(snackId, other.snackId)
				&& Objects.equals(snackName, other.snackName) && Objects.equals(startDate, other.startDate);
	}

}
