package hu.degreework.dps.api;

import java.util.Set;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RequestMapping(value = "/api")
@Tag(name = "Gender", description = "The Gender API")
public interface GenderApi {

	@Operation(summary = "Find Gender by id", description = "Returns Gender by the given id", tags = {
			"Gender" })
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation."),
			@ApiResponse(responseCode = "204", description = "Gender not found.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
	@GetMapping(value = "/gender/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	Gender getById(@PathVariable(value = "id") String id);

	@Operation(summary = "Get all Gender", description = "", tags = { "Gender" })
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
	@GetMapping(value = "/genders", produces = MediaType.APPLICATION_JSON_VALUE)
	Set<Gender> getAll();
}
