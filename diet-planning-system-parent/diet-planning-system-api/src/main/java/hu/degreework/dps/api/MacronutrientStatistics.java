package hu.degreework.dps.api;

import java.math.BigDecimal;
import java.util.Objects;

public class MacronutrientStatistics {

	private String id;
	private BigDecimal energy;
	private BigDecimal protein;
	private BigDecimal fat;
	private BigDecimal carbohydrate;
	private TimeIntervalType timeInterval;
	private String userId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BigDecimal getEnergy() {
		return energy;
	}

	public void setEnergy(BigDecimal energy) {
		this.energy = energy;
	}

	public BigDecimal getProtein() {
		return protein;
	}

	public void setProtein(BigDecimal protein) {
		this.protein = protein;
	}

	public BigDecimal getFat() {
		return fat;
	}

	public void setFat(BigDecimal fat) {
		this.fat = fat;
	}

	public BigDecimal getCarbohydrate() {
		return carbohydrate;
	}

	public void setCarbohydrate(BigDecimal carbohydrate) {
		this.carbohydrate = carbohydrate;
	}

	public TimeIntervalType getTimeInterval() {
		return timeInterval;
	}

	public void setTimeInterval(TimeIntervalType timeInterval) {
		this.timeInterval = timeInterval;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(carbohydrate, energy, fat, id, protein, timeInterval, userId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MacronutrientStatistics other = (MacronutrientStatistics) obj;
		return Objects.equals(carbohydrate, other.carbohydrate) && Objects.equals(energy, other.energy)
				&& Objects.equals(fat, other.fat) && Objects.equals(id, other.id)
				&& Objects.equals(protein, other.protein) && timeInterval == other.timeInterval
				&& Objects.equals(userId, other.userId);
	}

}
