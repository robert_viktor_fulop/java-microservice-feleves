package hu.degreework.dps.api;

import java.util.Set;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RequestMapping(value = "/api")
@Tag(name = "Food Material Fiber", description = "The food material API")
public interface FoodMaterialApi {

	@Operation(summary = "Find all Food Material", description = "Returns all Food Material", tags = { "FoodMaterial" })
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
    @GetMapping(value = "/food-materials", produces = MediaType.APPLICATION_JSON_VALUE)
    Set<FoodMaterial> getAll();
			
	@Operation(summary = "Find user food material by id", description = "Returns user food material by the given id", tags = {
    "User Anthropometers" })
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation."),
    @ApiResponse(responseCode = "404", description = "User Anthropometers not found.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
	@GetMapping(value = "/food-material/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	FoodMaterial getById(@PathVariable("id") String id);
	
}
