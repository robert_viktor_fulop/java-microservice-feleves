package hu.degreework.dps.api;

import java.util.Objects;

public class BodyMassIndex{

	private Double bmi;

	public Double getBmi() {
		return bmi;
	}

	public void setBmi(Double bmiValue) {
		this.bmi = bmiValue;
	}

	@Override
	public int hashCode() {
		return Objects.hash(bmi);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BodyMassIndex other = (BodyMassIndex) obj;
		return Objects.equals(bmi, other.bmi);
	}

}
