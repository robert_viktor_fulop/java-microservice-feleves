package hu.degreework.dps.api;

import java.util.Objects;

public class LunchGeneratedDiet {

	private GeneratedDiet soup;
	private GeneratedDiet mainDish;

	public GeneratedDiet getSoup() {
		return soup;
	}

	public void setSoup(GeneratedDiet soup) {
		this.soup = soup;
	}

	public GeneratedDiet getMainDish() {
		return mainDish;
	}

	public void setMainDish(GeneratedDiet mainDish) {
		this.mainDish = mainDish;
	}

	@Override
	public int hashCode() {
		return Objects.hash(mainDish, soup);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LunchGeneratedDiet other = (LunchGeneratedDiet) obj;
		return Objects.equals(mainDish, other.mainDish) && Objects.equals(soup, other.soup);
	}

}
