package hu.degreework.dps.api;

import java.util.Set;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RequestMapping(value = "/api")
@Tag(name = "Oil Preferences", description = "The Oil Preferences API")
public interface OilPreferencesApi {

	@Operation(summary = "Find all Oil Preferences", description = "Returns all Oil Preferences", tags = { "OilPreferences" })
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
    @GetMapping(value = "/oil-preferences", produces = MediaType.APPLICATION_JSON_VALUE)
    Set<OilPreferences> getAll();
			
	@Operation(summary = "Find Oil Preferences by id", description = "Returns Oil Preferences by the given id", tags = {
    "OilPreferences" })
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation."),
    @ApiResponse(responseCode = "404", description = "OilPreferences not found.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
	@GetMapping(value = "/oil-preferenc/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	OilPreferences getById(@PathVariable("id") String id);
	
}
