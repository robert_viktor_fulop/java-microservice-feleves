package hu.degreework.dps.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import java.util.stream.Stream;

public enum GenderType {
	NONE(0),
	MALE(1),
	FEMALE(2);

	private Integer code;
	
	private GenderType(Integer code) {
		this.code = code;
	}
	
	@JsonCreator
	public static GenderType decode(final Integer code) {
		return Stream.of(GenderType.values()).filter(targetEnum -> targetEnum.code.equals(code)).findFirst().orElse(null);
	}
	
	@JsonValue
	public Integer getCode() {
		return code;
	}
}
