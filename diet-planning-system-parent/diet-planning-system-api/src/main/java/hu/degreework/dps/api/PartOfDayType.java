package hu.degreework.dps.api;

import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum PartOfDayType {
	NONE(0),
	BREAKFAST(1),
	BRUNCH(2),
	LUNCH(3),
	SNACK(4),
	DINNER(5);
	
	private Integer code;
	
	private PartOfDayType(Integer code) {
		this.code = code;
	}
	
	@JsonCreator
	public static PartOfDayType decode(final Integer code) {
		return Stream.of(PartOfDayType.values()).filter(targetEnum -> targetEnum.code.equals(code)).findFirst().orElse(null);
	}
	
	@JsonValue
	public Integer getCode() {
		return code;
	}
}
