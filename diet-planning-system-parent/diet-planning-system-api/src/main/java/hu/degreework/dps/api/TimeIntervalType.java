package hu.degreework.dps.api;

import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum TimeIntervalType {

	NONE(0),
	IMMEDIATELY(1),
	MONTHLY(2);

	
	private Integer code;
	
	private TimeIntervalType(Integer code) {
		this.code = code;
	}
	
	@JsonCreator
	public static TimeIntervalType decode(final Integer code) {
		return Stream.of(TimeIntervalType.values()).filter(targetEnum -> targetEnum.code.equals(code)).findFirst().orElse(null);
	}
	
	@JsonValue
	public Integer getCode() {
		return code;
	}
}
