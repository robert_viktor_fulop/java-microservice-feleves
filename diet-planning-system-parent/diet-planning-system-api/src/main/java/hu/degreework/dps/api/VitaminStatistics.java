package hu.degreework.dps.api;

import java.math.BigDecimal;
import java.util.Objects;

public class VitaminStatistics {

	private String id;
	private BigDecimal aVitamin;
	private BigDecimal kVitamin;
	private BigDecimal dVitamin;
	private BigDecimal cVitamin;
	private BigDecimal bOneVitamin;
	private BigDecimal bTwoVitamin;
	private TimeIntervalType timeInterval;
	private String userId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BigDecimal getaVitamin() {
		return aVitamin;
	}

	public void setaVitamin(BigDecimal aVitamin) {
		this.aVitamin = aVitamin;
	}

	public BigDecimal getkVitamin() {
		return kVitamin;
	}

	public void setkVitamin(BigDecimal kVitamin) {
		this.kVitamin = kVitamin;
	}

	public BigDecimal getdVitamin() {
		return dVitamin;
	}

	public void setdVitamin(BigDecimal dVitamin) {
		this.dVitamin = dVitamin;
	}

	public BigDecimal getcVitamin() {
		return cVitamin;
	}

	public void setcVitamin(BigDecimal cVitamin) {
		this.cVitamin = cVitamin;
	}

	public BigDecimal getbOneVitamin() {
		return bOneVitamin;
	}

	public void setbOneVitamin(BigDecimal bOneVitamin) {
		this.bOneVitamin = bOneVitamin;
	}

	public BigDecimal getbTwoVitamin() {
		return bTwoVitamin;
	}

	public void setbTwoVitamin(BigDecimal bTwoVitamin) {
		this.bTwoVitamin = bTwoVitamin;
	}

	public TimeIntervalType getTimeInterval() {
		return timeInterval;
	}

	public void setTimeInterval(TimeIntervalType timeInterval) {
		this.timeInterval = timeInterval;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(aVitamin, bOneVitamin, bTwoVitamin, cVitamin, dVitamin, id, kVitamin, timeInterval, userId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VitaminStatistics other = (VitaminStatistics) obj;
		return Objects.equals(aVitamin, other.aVitamin) && Objects.equals(bOneVitamin, other.bOneVitamin)
				&& Objects.equals(bTwoVitamin, other.bTwoVitamin) && Objects.equals(cVitamin, other.cVitamin)
				&& Objects.equals(dVitamin, other.dVitamin) && Objects.equals(id, other.id)
				&& Objects.equals(kVitamin, other.kVitamin) && timeInterval == other.timeInterval
				&& Objects.equals(userId, other.userId);
	}

}
