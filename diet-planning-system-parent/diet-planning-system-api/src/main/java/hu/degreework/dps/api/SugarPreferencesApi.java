package hu.degreework.dps.api;

import java.util.Set;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RequestMapping(value = "/api")
@Tag(name = "Sugar Preferences", description = "The Sugar Preferences API")
public interface SugarPreferencesApi {

	@Operation(summary = "Find all Sugar Preferences", description = "Returns all Sugar Preferences", tags = { "SugarPreferences" })
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
    @GetMapping(value = "/sugar-preferences", produces = MediaType.APPLICATION_JSON_VALUE)
    Set<SugarPreferences> getAll();
			
	@Operation(summary = "Find Sugar Preferences by id", description = "Returns Sugar Preferences  by the given id", tags = {
    "SugarPreferences" })
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation."),
    @ApiResponse(responseCode = "404", description = "SugarPreferences not found.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
	@GetMapping(value = "/sugar-preferences/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	SugarPreferences getById(@PathVariable("id") String id);
	
}
