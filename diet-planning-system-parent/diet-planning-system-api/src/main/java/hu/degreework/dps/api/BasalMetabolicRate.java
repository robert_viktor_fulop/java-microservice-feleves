package hu.degreework.dps.api;

import java.util.Objects;

public class BasalMetabolicRate {
	
	private Double bmr;

	public Double getBmr() {
		return bmr;
	}

	public void setBmr(Double bmr) {
		this.bmr = bmr;
	}

	@Override
	public int hashCode() {
		return Objects.hash(bmr);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BasalMetabolicRate other = (BasalMetabolicRate) obj;
		return Double.doubleToLongBits(bmr) == Double.doubleToLongBits(other.bmr);
	}
	
}
