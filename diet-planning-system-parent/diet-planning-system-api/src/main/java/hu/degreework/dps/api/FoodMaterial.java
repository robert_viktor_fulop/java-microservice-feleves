package hu.degreework.dps.api;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.Set;

public class FoodMaterial {

	private String id;
	private String name;
	private String category;
	private String subcategory;
	private BigDecimal energyKcal;
	private BigDecimal proteinGram;
	private BigDecimal fatGram;
	private BigDecimal carbohydrateGram;
	private String gi;
	private Set<String> vitaminInFoodMaterials;
	private Set<String> fibers;
	private Set<String> cholesterols;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getSubcategory() {
		return subcategory;
	}

	public void setSubcategory(String subcategory) {
		this.subcategory = subcategory;
	}

	public BigDecimal getEnergyKcal() {
		return energyKcal;
	}

	public void setEnergyKcal(BigDecimal energyKcal) {
		this.energyKcal = energyKcal;
	}

	public BigDecimal getProteinGram() {
		return proteinGram;
	}

	public void setProteinGram(BigDecimal proteinGram) {
		this.proteinGram = proteinGram;
	}

	public BigDecimal getFatGram() {
		return fatGram;
	}

	public void setFatGram(BigDecimal fatGram) {
		this.fatGram = fatGram;
	}

	public BigDecimal getCarbohydrateGram() {
		return carbohydrateGram;
	}

	public void setCarbohydrateGram(BigDecimal carbohydrateGram) {
		this.carbohydrateGram = carbohydrateGram;
	}

	public String getGi() {
		return gi;
	}

	public void setGi(String gi) {
		this.gi = gi;
	}

	public Set<String> getVitaminInFoodMaterials() {
		return vitaminInFoodMaterials;
	}

	public void setVitaminInFoodMaterials(Set<String> vitaminInFoodMaterials) {
		this.vitaminInFoodMaterials = vitaminInFoodMaterials;
	}

	public Set<String> getFibers() {
		return fibers;
	}

	public void setFibers(Set<String> fibers) {
		this.fibers = fibers;
	}

	public Set<String> getCholesterols() {
		return cholesterols;
	}

	public void setCholesterols(Set<String> cholesterols) {
		this.cholesterols = cholesterols;
	}

	@Override
	public int hashCode() {
		return Objects.hash(carbohydrateGram, category, cholesterols, energyKcal, fatGram, fibers, gi, id, name,
				proteinGram, subcategory, vitaminInFoodMaterials);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FoodMaterial other = (FoodMaterial) obj;
		return Objects.equals(carbohydrateGram, other.carbohydrateGram) && Objects.equals(category, other.category)
				&& Objects.equals(cholesterols, other.cholesterols) && Objects.equals(energyKcal, other.energyKcal)
				&& Objects.equals(fatGram, other.fatGram) && Objects.equals(fibers, other.fibers)
				&& Objects.equals(gi, other.gi) && Objects.equals(id, other.id) && Objects.equals(name, other.name)
				&& Objects.equals(proteinGram, other.proteinGram) && Objects.equals(subcategory, other.subcategory)
				&& Objects.equals(vitaminInFoodMaterials, other.vitaminInFoodMaterials);
	}
}