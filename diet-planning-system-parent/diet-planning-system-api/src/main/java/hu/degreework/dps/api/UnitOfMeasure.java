package hu.degreework.dps.api;

import java.util.Objects;
import java.util.Set;

public class UnitOfMeasure {

	private String id;
	private String name;
	private Set<String> ingredientIds;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<String> getIngredientIds() {
		return ingredientIds;
	}

	public void setIngredientIds(Set<String> ingredientIds) {
		this.ingredientIds = ingredientIds;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, ingredientIds, name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnitOfMeasure other = (UnitOfMeasure) obj;
		return Objects.equals(id, other.id) && Objects.equals(ingredientIds, other.ingredientIds)
				&& Objects.equals(name, other.name);
	}
}
