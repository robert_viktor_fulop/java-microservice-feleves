package hu.degreework.dps.api;

import java.util.Objects;
import java.util.Set;

public class OilPreferences {

	private String id;
	private OilPreferencesType name;
	private Set<String> recipesId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public OilPreferencesType getName() {
		return name;
	}

	public void setName(OilPreferencesType name) {
		this.name = name;
	}

	public Set<String> getRecipesId() {
		return recipesId;
	}

	public void setRecipesId(Set<String> recipesId) {
		this.recipesId = recipesId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, recipesId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OilPreferences other = (OilPreferences) obj;
		return Objects.equals(id, other.id) && name == other.name && Objects.equals(recipesId, other.recipesId);
	}

}
