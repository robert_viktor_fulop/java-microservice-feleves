package hu.degreework.dps.api;

import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum MealPreferenceType {
	NONE(0),
	NO_SELECTION(1),
	NO_BRUNCH(2),
	NO_SNACK(3);
	
	private Integer code;
	
	private MealPreferenceType(Integer code) {
		this.code = code;
	}
	
	@JsonCreator
	public static MealPreferenceType decode(final Integer code) {
		return Stream.of(MealPreferenceType.values()).filter(targetEnum -> targetEnum.code.equals(code)).findFirst().orElse(null);
	}
	
	@JsonValue
	public Integer getCode() {
		return code;
	}
}
