package hu.degreework.dps.api;

import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum KatchMcArdleType {
	NONE(0),
	SEDENTARINESS(1),
	MODERATE_EXERCISE(2),
	ACTIVE_EXERCISE(3),
	VERY_ACTIVE_EXERCISE(4);
	
	private Integer code;
	
	private KatchMcArdleType(Integer code) {
		this.code = code;
	}
	
	@JsonCreator
	public static KatchMcArdleType decode(final Integer code) {
		return Stream.of(KatchMcArdleType.values()).filter(targetEnum -> targetEnum.code.equals(code)).findFirst().orElse(null);
	}
	
	@JsonValue
	public Integer getCode() {
		return code;
	}
}
