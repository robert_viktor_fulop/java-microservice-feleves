package hu.degreework.dps.api;

import java.util.List;
import java.util.Set;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RequestMapping(value = "/api")
@Tag(name = "DietCalendar", description = "Save Diet planned in calendar API")
public interface DietCalendarApi {

	@Operation(summary = "DietCalendar", description = "Save Diet planned in calendar", tags = { "DietCalendar" })
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
	@PostMapping(value = "/planned-diets/calendar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	void save(@RequestBody List<DietCalendar> dietCalendar, Authentication anAuthentication);
	
	@Operation(summary = "Find all DietCalendar", description = "Returns all DietCalendar", tags = {"DietCalendar"})
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
    @GetMapping(value = "/planned-diets/calendar", produces = MediaType.APPLICATION_JSON_VALUE)
	List<DietCalendar> getAll(Authentication anAuthentication);
}
