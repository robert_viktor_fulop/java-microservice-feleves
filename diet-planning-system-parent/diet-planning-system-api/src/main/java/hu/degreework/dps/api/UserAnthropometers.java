package hu.degreework.dps.api;

import java.util.Date;
import java.util.Objects;

public class UserAnthropometers {

	private String id;
	private KatchMcArdleType katchMcArdle;
	private Date birthday;
	private GenderType gender;
	private Integer age;
	private String activity;
	private Double weight;
	private Double height;
	private Double waist;
	private Double forearm;
	private Double wrist;
	private Double hip;
	private Double bmr;
	private Double bfp;
	private Double bfw;
	private Double lbmFemale;
	private Double lbmMale;
	private Double bmi;
	private Date timestamp;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public KatchMcArdleType getKatchMcArdle() {
		return katchMcArdle;
	}

	public void setKatchMcArdle(KatchMcArdleType katchMcArdle) {
		this.katchMcArdle = katchMcArdle;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public GenderType getGender() {
		return gender;
	}

	public void setGender(GenderType gender) {
		this.gender = gender;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getHeight() {
		return height;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	public Double getWaist() {
		return waist;
	}

	public void setWaist(Double waist) {
		this.waist = waist;
	}

	public Double getForearm() {
		return forearm;
	}

	public void setForearm(Double forearm) {
		this.forearm = forearm;
	}

	public Double getWrist() {
		return wrist;
	}

	public void setWrist(Double wrist) {
		this.wrist = wrist;
	}

	public Double getHip() {
		return hip;
	}

	public void setHip(Double hip) {
		this.hip = hip;
	}

	public Double getBmr() {
		return bmr;
	}

	public void setBmr(Double bmr) {
		this.bmr = bmr;
	}

	public Double getBfp() {
		return bfp;
	}

	public void setBfp(Double bfp) {
		this.bfp = bfp;
	}

	public Double getBfw() {
		return bfw;
	}

	public void setBfw(Double bfw) {
		this.bfw = bfw;
	}

	public Double getLbmFemale() {
		return lbmFemale;
	}

	public void setLbmFemale(Double lbmFemale) {
		this.lbmFemale = lbmFemale;
	}

	public Double getLbmMale() {
		return lbmMale;
	}

	public void setLbmMale(Double lbmMale) {
		this.lbmMale = lbmMale;
	}

	public Double getBmi() {
		return bmi;
	}

	public void setBmi(Double bmi) {
		this.bmi = bmi;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public int hashCode() {
		return Objects.hash(activity, age, bfp, bfw, birthday, bmi, bmr, forearm, gender, height, hip, id,
				katchMcArdle, lbmFemale, lbmMale, timestamp, waist, weight, wrist);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserAnthropometers other = (UserAnthropometers) obj;
		return Objects.equals(activity, other.activity) && Objects.equals(age, other.age)
				&& Objects.equals(bfp, other.bfp) && Objects.equals(bfw, other.bfw)
				&& Objects.equals(birthday, other.birthday) && Objects.equals(bmi, other.bmi)
				&& Objects.equals(bmr, other.bmr) && Objects.equals(forearm, other.forearm)
				&& gender == other.gender && Objects.equals(height, other.height) && Objects.equals(hip, other.hip)
				&& Objects.equals(id, other.id) && katchMcArdle == other.katchMcArdle
				&& Objects.equals(lbmFemale, other.lbmFemale) && Objects.equals(lbmMale, other.lbmMale)
				&& Objects.equals(timestamp, other.timestamp) && Objects.equals(waist, other.waist)
				&& Objects.equals(weight, other.weight) && Objects.equals(wrist, other.wrist);
	}

}
