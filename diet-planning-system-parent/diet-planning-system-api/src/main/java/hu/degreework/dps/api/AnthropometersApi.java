package hu.degreework.dps.api;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/api")
public interface AnthropometersApi {
	
	@PostMapping(value = "/anthropometers/bmi", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	BodyMassIndex bmiCalculation(@RequestBody Anthropometers bmi);
	
	@PostMapping(value = "/anthropometers/energy-demand", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	EnergyDemand energyDemandCalculation(@RequestBody Anthropometers anthropometers);
	
	@PostMapping(value = "/anthropometers/body-fat-percentage", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	BodyFatPercentage bodyFatPercentageCalculation(@RequestBody Anthropometers anthropometers);
}
