package hu.degreework.dps.api;

import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum TargetType {

	NONE(0),
	WEIGHT_LOSS(1),
	WEIGHT_MAINTAIN(2),
	WEIGHT_GAIN(3);
	
	private Integer code;
	
	private TargetType(Integer code) {
		this.code = code;
	}
	
	@JsonCreator
	public static TargetType decode(final Integer code) {
		return Stream.of(TargetType.values()).filter(targetEnum -> targetEnum.code.equals(code)).findFirst().orElse(null);
	}
	
	@JsonValue
	public Integer getCode() {
		return code;
	}
}
