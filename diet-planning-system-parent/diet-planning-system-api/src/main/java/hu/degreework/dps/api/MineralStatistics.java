package hu.degreework.dps.api;

import java.math.BigDecimal;
import java.util.Objects;

public class MineralStatistics {

	private String id;
	private BigDecimal iron;
	private BigDecimal calcium;
	private BigDecimal magnesium;
	private TimeIntervalType timeInterval;
	private String userId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BigDecimal getIron() {
		return iron;
	}

	public void setIron(BigDecimal iron) {
		this.iron = iron;
	}

	public BigDecimal getCalcium() {
		return calcium;
	}

	public void setCalcium(BigDecimal calcium) {
		this.calcium = calcium;
	}

	public BigDecimal getMagnesium() {
		return magnesium;
	}

	public void setMagnesium(BigDecimal magnesium) {
		this.magnesium = magnesium;
	}

	public TimeIntervalType getTimeInterval() {
		return timeInterval;
	}

	public void setTimeInterval(TimeIntervalType timeInterval) {
		this.timeInterval = timeInterval;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(calcium, id, iron, magnesium, timeInterval, userId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MineralStatistics other = (MineralStatistics) obj;
		return Objects.equals(calcium, other.calcium) && Objects.equals(id, other.id)
				&& Objects.equals(iron, other.iron) && Objects.equals(magnesium, other.magnesium)
				&& timeInterval == other.timeInterval && Objects.equals(userId, other.userId);
	}

}
