package hu.degreework.dps.api;

import java.util.Objects;

public class GeneratedDiets {

	private String id;
	private GeneratedDiet breakfast;
	private GeneratedDiet brunch;
	private LunchGeneratedDiet lunch;
	private GeneratedDiet snack;
	private GeneratedDiet dinner;
	private Double dailyCalorieIntake;
	private Integer number;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public GeneratedDiet getBreakfast() {
		return breakfast;
	}

	public void setBreakfast(GeneratedDiet breakfast) {
		this.breakfast = breakfast;
	}

	public GeneratedDiet getBrunch() {
		return brunch;
	}

	public void setBrunch(GeneratedDiet brunch) {
		this.brunch = brunch;
	}

	public LunchGeneratedDiet getLunch() {
		return lunch;
	}

	public void setLunch(LunchGeneratedDiet lunch) {
		this.lunch = lunch;
	}

	public GeneratedDiet getSnack() {
		return snack;
	}

	public void setSnack(GeneratedDiet snack) {
		this.snack = snack;
	}

	public GeneratedDiet getDinner() {
		return dinner;
	}

	public void setDinner(GeneratedDiet dinner) {
		this.dinner = dinner;
	}

	public Double getDailyCalorieIntake() {
		return dailyCalorieIntake;
	}

	public void setDailyCalorieIntake(Double dailyCalorieIntake) {
		this.dailyCalorieIntake = dailyCalorieIntake;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	@Override
	public int hashCode() {
		return Objects.hash(breakfast, brunch, dailyCalorieIntake, dinner, id, lunch, number, snack);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GeneratedDiets other = (GeneratedDiets) obj;
		return Objects.equals(breakfast, other.breakfast) && Objects.equals(brunch, other.brunch)
				&& Objects.equals(dailyCalorieIntake, other.dailyCalorieIntake) && Objects.equals(dinner, other.dinner)
				&& Objects.equals(id, other.id) && Objects.equals(lunch, other.lunch)
				&& Objects.equals(number, other.number) && Objects.equals(snack, other.snack);
	}

}
