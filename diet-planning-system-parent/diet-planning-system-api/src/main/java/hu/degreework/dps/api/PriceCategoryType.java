package hu.degreework.dps.api;

import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum PriceCategoryType {
	NONE(0),
	LOW(1),
	NORMAL(2),
	HIGH(3);
	
	private Integer code;
	
	private PriceCategoryType(Integer code) {
		this.code = code;
	}
	
	@JsonCreator
	public static PriceCategoryType decode(final Integer code) {
		return Stream.of(PriceCategoryType.values()).filter(targetEnum -> targetEnum.code.equals(code)).findFirst().orElse(null);
	}
	
	@JsonValue
	public Integer getCode() {
		return code;
	}
}
