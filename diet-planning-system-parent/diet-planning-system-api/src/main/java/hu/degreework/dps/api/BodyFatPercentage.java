package hu.degreework.dps.api;

public class BodyFatPercentage {
	
	private Double bfp;
	private Double bfw;
	private Double lbmFemale;
	private Double lbmMale;
	
	public Double  getBfp() {
		return bfp;
	}
	public void setBfp(Double  bfp) {
		this.bfp = bfp;
	}
	public Double  getBfw() {
		return bfw;
	}
	public void setBfw(Double bfw) {
		this.bfw = bfw;
	}
	public Double  getLbmFemale() {
		return lbmFemale;
	}
	public void setLbmFemale(Double lbmFemale) {
		this.lbmFemale = lbmFemale;
	}
	public Double  getLbmMale() {
		return lbmMale;
	}
	public void setLbmMale(Double lbmMale) {
		this.lbmMale = lbmMale;
	}
	
}
