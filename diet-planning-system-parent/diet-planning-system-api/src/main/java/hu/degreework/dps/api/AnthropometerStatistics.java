package hu.degreework.dps.api;

import java.math.BigDecimal;
import java.util.Objects;

public class AnthropometerStatistics {

	private String id;
	private BigDecimal bmi;
	private BigDecimal bmr;
	private BigDecimal bfp;
	private BigDecimal bfw;
	private BigDecimal lbm;
	private TimeIntervalType timeInterval;
	private String userId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BigDecimal getBmi() {
		return bmi;
	}

	public void setBmi(BigDecimal bmi) {
		this.bmi = bmi;
	}

	public BigDecimal getBmr() {
		return bmr;
	}

	public void setBmr(BigDecimal bmr) {
		this.bmr = bmr;
	}

	public BigDecimal getBfp() {
		return bfp;
	}

	public void setBfp(BigDecimal bfp) {
		this.bfp = bfp;
	}

	public BigDecimal getBfw() {
		return bfw;
	}

	public void setBfw(BigDecimal bfw) {
		this.bfw = bfw;
	}

	public BigDecimal getLbm() {
		return lbm;
	}

	public void setLbm(BigDecimal lbm) {
		this.lbm = lbm;
	}

	public TimeIntervalType getTimeInterval() {
		return timeInterval;
	}

	public void setTimeInterval(TimeIntervalType timeInterval) {
		this.timeInterval = timeInterval;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(bfp, bfw, bmi, bmr, id, lbm, timeInterval, userId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnthropometerStatistics other = (AnthropometerStatistics) obj;
		return Objects.equals(bfp, other.bfp) && Objects.equals(bfw, other.bfw) && Objects.equals(bmi, other.bmi)
				&& Objects.equals(bmr, other.bmr) && Objects.equals(id, other.id) && Objects.equals(lbm, other.lbm)
				&& timeInterval == other.timeInterval && Objects.equals(userId, other.userId);
	}

}
