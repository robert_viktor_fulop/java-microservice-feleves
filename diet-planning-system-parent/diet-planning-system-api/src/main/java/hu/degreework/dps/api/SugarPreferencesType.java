package hu.degreework.dps.api;

import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum SugarPreferencesType {
	NONE(0),
	NO_SELECTION(1),
	NO_SUGAR(2),
	REDUCED(3),
	LIMITED(4);
	
	private Integer code;
	
	private SugarPreferencesType(Integer code) {
		this.code = code;
	}
	
	@JsonCreator
	public static SugarPreferencesType decode(final Integer code) {
		return Stream.of(SugarPreferencesType.values()).filter(targetEnum -> targetEnum.code.equals(code)).findFirst().orElse(null);
	}
	
	@JsonValue
	public Integer getCode() {
		return code;
	}
}
