package hu.degreework.dps.api;

import java.util.Objects;

public class EnergyDemand {

	//basal Metabolic Rate
	private Double bmrMin;
	private Double totalDailyEenergyConsumption;
	
	public Double getBmrMin() {
		return bmrMin;
	}
	public void setBmrMin(Double bmrMin) {
		this.bmrMin = bmrMin;
	}
	public Double getTotalDailyEenergyConsumption() {
		return totalDailyEenergyConsumption;
	}
	public void setTotalDailyEenergyConsumption(Double totalDailyEenergyConsumption) {
		this.totalDailyEenergyConsumption = totalDailyEenergyConsumption;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(bmrMin, totalDailyEenergyConsumption);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EnergyDemand other = (EnergyDemand) obj;
		return Double.doubleToLongBits(bmrMin) == Double.doubleToLongBits(other.bmrMin)
				&& Double.doubleToLongBits(totalDailyEenergyConsumption) == Double
						.doubleToLongBits(other.totalDailyEenergyConsumption);
	}
	
}
