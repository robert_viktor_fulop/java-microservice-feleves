package hu.degreework.dps.api;

import java.util.Objects;

public class Anthropometers {
	
	private GenderType gender;
	private KatchMcArdleType katchMcArdle;
	private Integer age;
	private String activity;
	private Double weight;
	private Double height;
	private Double waist;
	private Double forearm;
	private Double wrist;
	private Double hip;
	
	public GenderType getGender() {
		return gender;
	}
	public void setGender(GenderType gender) {
		this.gender = gender;
	}
	public KatchMcArdleType getKatchMcArdle() {
		return katchMcArdle;
	}
	public void setKatchMcArdle(KatchMcArdleType katchMcArdle) {
		this.katchMcArdle = katchMcArdle;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	public Double getHeight() {
		return height;
	}
	public void setHeight(Double height) {
		this.height = height;
	}
	public Double getWaist() {
		return waist;
	}
	public void setWaist(Double waist) {
		this.waist = waist;
	}
	public Double getForearm() {
		return forearm;
	}
	public void setForearm(Double forearm) {
		this.forearm = forearm;
	}
	public Double getWrist() {
		return wrist;
	}
	public void setWrist(Double wrist) {
		this.wrist = wrist;
	}
	public Double getHip() {
		return hip;
	}
	public void setHip(Double hip) {
		this.hip = hip;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(activity, age, forearm, gender, height, hip, katchMcArdle, waist, weight, wrist);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Anthropometers other = (Anthropometers) obj;
		return Objects.equals(activity, other.activity) && Objects.equals(age, other.age)
				&& Objects.equals(forearm, other.forearm) && gender == other.gender
				&& Objects.equals(height, other.height) && Objects.equals(hip, other.hip)
				&& katchMcArdle == other.katchMcArdle && Objects.equals(waist, other.waist)
				&& Objects.equals(weight, other.weight) && Objects.equals(wrist, other.wrist);
	}
		
}
