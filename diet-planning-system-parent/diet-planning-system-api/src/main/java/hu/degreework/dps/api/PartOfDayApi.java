package hu.degreework.dps.api;

import java.util.Set;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RequestMapping(value = "/api")
@Tag(name = "Part Of Day", description = "The Part Of Day API")
public interface PartOfDayApi {

	@Operation(summary = "Find all Part Of Day", description = "Returns all Part Of Day", tags = { "PartOfDay" })
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
    @GetMapping(value = "/parts-of-day", produces = MediaType.APPLICATION_JSON_VALUE)
    Set<PartOfDay> getAll();
			
	@Operation(summary = "Find Part Of Day by id", description = "Returns Part Of Day by the given id", tags = {
    "PartOfDay" })
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation."),
    @ApiResponse(responseCode = "404", description = "PartOfDay not found.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
	@GetMapping(value = "/part-of-day/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	PartOfDay getById(@PathVariable("id") String id);
	
}
