package hu.degreework.dps.api;

import java.util.Set;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RequestMapping(value = "/api")
@Tag(name = "Vitamin Statistics", description = "The Vitamin Statistics API")
public interface VitaminStatisticsApi {

	@Operation(summary = "Find all Vitamin Statistics", description = "Returns all Vitamin Statistics", tags = { "FoodMaterial" })
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
    @GetMapping(value = "/vitamin-statistics", produces = MediaType.APPLICATION_JSON_VALUE)
    Set<VitaminStatistics> getAll(Authentication anAuthentication);
			
	@Operation(summary = "Find Vitamin Statistics by id", description = "Returns Vitamin statistics by the given id", tags = {
    "Vitamin Statistics" })
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation."),
    @ApiResponse(responseCode = "404", description = "Vitamin Statistics not found.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
	@GetMapping(value = "/vitamin-statistics/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	VitaminStatistics getById(@PathVariable("id") String id, Authentication anAuthentication);
}
