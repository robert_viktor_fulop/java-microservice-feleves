package hu.degreework.dps.api;

import java.util.Set;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RequestMapping(value = "/api")
@Tag(name = "Macronutrient Statistics", description = "The Macronutrient Statistics API")
public interface MacronutrientStatisticsApi {

	@Operation(summary = "Find all Macronutrient Statistics", description = "Returns all Macronutrient Statistics", tags = { "MacronutrientStatistics" })
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
    @GetMapping(value = "/macronutrient-statistics", produces = MediaType.APPLICATION_JSON_VALUE)
    Set<MacronutrientStatistics> getAll(Authentication anAuthentication);
			
	@Operation(summary = "Find Macronutrient Statistics by id", description = "Returns Macronutrient Statistics by the given id", tags = {
    "MacronutrientStatistics" })
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation."),
    @ApiResponse(responseCode = "404", description = "MacronutrientStatistics not found.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
	@GetMapping(value = "/macronutrient-statistics/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	MacronutrientStatistics getById(@PathVariable("id") String id, Authentication anAuthentication);
	
}
