package hu.degreework.dps.api;

import java.util.Set;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RequestMapping(value = "/api")
@Tag(name = "Unit Of MeasureApi", description = "The Unit Of MeasureApi API")
public interface UnitOfMeasureApi {

	@Operation(summary = "Find all Unit Of MeasureApi", description = "Returns all Unit Of MeasureApi", tags = { "UnitOfMeasureApi" })
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
    @GetMapping(value = "/unit-of-measures", produces = MediaType.APPLICATION_JSON_VALUE)
    Set<UnitOfMeasure> getAll();
			
	@Operation(summary = "Find Unit Of Measure by id", description = "Returns Unit Of MeasureApi by the given id", tags = {
    "Unit Of Measure" })
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation."),
    @ApiResponse(responseCode = "404", description = "Unit Of Measure not found.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
	@GetMapping(value = "/unit-of-measure/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	UnitOfMeasure getById(@PathVariable("id") String id);
}
