package hu.degreework.dps.api;

import java.util.List;
import java.util.Set;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RequestMapping(value = "/api")
@Tag(name = "DietGenerate", description = "The DietGenerate API")
public interface DietGenerateApi {

	@Operation(summary = "Generated DietGenerate", description = "Returns all DietGenerate", tags = { "DietGenerate" })
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
    @PostMapping(value = "/diet-generation",  consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	List<GeneratedDiets> generateStart(@RequestBody DietGenerate dietGenerate, Authentication anAuthentication);
	
	@Operation(summary = "Find all DietGenerate", description = "Returns all DietGenerate", tags = { "DietGenerate" })
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
    @PostMapping(value = "/diet-generation/save",  consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	void saveGeneratedDiets(@RequestBody List<GeneratedDiets> generatedDiets, Authentication anAuthentication);
}
