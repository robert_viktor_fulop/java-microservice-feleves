package hu.degreework.dps.api;

import java.util.List;
import java.util.Set;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RequestMapping(value = "/api")
@Tag(name = "Recipe", description = "The Recipe API")
public interface RecipeApi {

	@Operation(summary = "Find all Recipe", description = "Returns all Recipe", tags = { "Recipe" })
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
    @GetMapping(value = "/recipes", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<Set<Recipe>> getAll( 
			@RequestParam(defaultValue = "0") Integer pageNo, 
            @RequestParam(defaultValue = "10") Integer pageSize);
	
	@Operation(summary = "Find all Recipe", description = "Returns all Recipe", tags = { "Recipe" })
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
    @GetMapping(value = "/all-recipes", produces = MediaType.APPLICATION_JSON_VALUE)
	Set<Recipe> getAllWithoutPages();
	
	@Operation(summary = "Find Recipe by id", description = "Returns Recipe by the given id", tags = {
    "Recipe" })
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation."),
    @ApiResponse(responseCode = "404", description = "Recipe not found.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
	@GetMapping(value = "/recipe/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	Recipe getById(@PathVariable("id") String id);
		
}
