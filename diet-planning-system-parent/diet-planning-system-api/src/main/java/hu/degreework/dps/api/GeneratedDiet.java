package hu.degreework.dps.api;

import java.math.BigDecimal;
import java.util.Objects;

public class GeneratedDiet {

	private String id;
	private String name;
	private BigDecimal calorie;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getCalorie() {
		return calorie;
	}

	public void setCalorie(BigDecimal calorie) {
		this.calorie = calorie;
	}

	@Override
	public int hashCode() {
		return Objects.hash(calorie, id, name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GeneratedDiet other = (GeneratedDiet) obj;
		return Objects.equals(calorie, other.calorie) && Objects.equals(id, other.id)
				&& Objects.equals(name, other.name);
	}
}
