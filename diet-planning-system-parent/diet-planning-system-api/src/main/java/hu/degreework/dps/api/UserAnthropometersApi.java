package hu.degreework.dps.api;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RequestMapping(value = "/api")
@Tag(name = "User Anthropometers", description = "The user anthropometers API")
public interface UserAnthropometersApi {

	@Operation(summary = "Create new User Anthropometers", description = "", tags = { "User Anthropometers" })
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
	@PostMapping(value = "/user-anthropometers/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	UserAnthropometers add(@RequestBody UserAnthropometers fiber, Authentication anAuthentication);

	@Operation(summary = "Find user anthropometers by id", description = "Returns user anthropometers by the given id", tags = {
			"User Anthropometers" })
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation."),
			@ApiResponse(responseCode = "204", description = "User Anthropometers not found.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
	@GetMapping(value = "/user-anthropometers", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody()
	UserAnthropometers getAnthropometers(Authentication anAuthentication);

	@Operation(summary = "Update User Anthropometers", description = "", tags = { "User Anthropometers" })
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
	@PutMapping(value = "/user-anthropometers/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	UserAnthropometers update(@RequestBody UserAnthropometers fiber, Authentication anAuthentication);

}
