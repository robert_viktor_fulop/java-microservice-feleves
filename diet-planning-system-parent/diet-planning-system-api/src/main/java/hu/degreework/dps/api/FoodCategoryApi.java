package hu.degreework.dps.api;

import java.util.List;
import java.util.Set;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/api")
public interface FoodCategoryApi {
	
	@PreAuthorize("hasRole('REGISTERED_USER')")
	@GetMapping(value = "/food-category", produces = MediaType.APPLICATION_JSON_VALUE)
	Set<FoodCategory> getAll();
	
	@PreAuthorize("hasRole('REGISTERED_USER')")
	@PostMapping(value = "/food-category/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	FoodCategory addFoodCategory(@RequestBody FoodCategory foodCategory);
	
	@PreAuthorize("hasRole('REGISTERED_USER')")
	@PostMapping(value = "/food-category/create/all", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	List<FoodCategory> addAllFoodCategory(@RequestBody List<FoodCategory> foodCategories);
	
	@PreAuthorize("hasRole('REGISTERED_USER')")
	@GetMapping(value = "/food-category/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	FoodCategory getFoodCategoryById(@PathVariable("id") String id);
	
	@PreAuthorize("hasRole('REGISTERED_USER')")
	@DeleteMapping(value = "/food-category/{id}")
	void deleteFoodCategory(@PathVariable("id") String id);
}
