package hu.degreework.dps.api;

import java.util.Set;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RequestMapping(value = "/api")
@Tag(name = "User Diet", description = "The user diet API")
public interface UserDietApi {

	@Operation(summary = "Create new User Anthropometers", description = "", tags = { "User Anthropometers" })
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
	@PostMapping(value = "/user-anthropometers/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	UserDiet add(@RequestBody UserDiet fiber, Authentication anAuthentication);
	
	@Operation(summary = "Find all User Diet", description = "Returns all User Diet", tags = { "UserDiet" })
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
    @GetMapping(value = "/user-diets", produces = MediaType.APPLICATION_JSON_VALUE)
    Set<UserDiet> getAll(Authentication anAuthentication);
			
	@Operation(summary = "Find User Diet by id", description = "Returns User Diet by the given id", tags = {
    "UserDiet" })
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successful operation."),
    @ApiResponse(responseCode = "404", description = "UserDiet not found.") })
	@PreAuthorize("hasRole('REGISTERED_USER')")
	@GetMapping(value = "/user-diet/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	UserDiet getById(@PathVariable("id") String id, Authentication anAuthentication);
	
}
