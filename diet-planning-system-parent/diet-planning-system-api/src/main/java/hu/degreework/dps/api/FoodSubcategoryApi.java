package hu.degreework.dps.api;

import java.util.List;
import java.util.Set;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/api")
public interface FoodSubcategoryApi {

	@PreAuthorize("hasRole('REGISTERED_USER')")
	@GetMapping(value = "/food-subcategory", produces = MediaType.APPLICATION_JSON_VALUE)
	Set<FoodSubcategory> getAll();
	
	@PreAuthorize("hasRole('REGISTERED_USER')")
	@PostMapping(value = "/food-subcategory/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	FoodSubcategory addFoodSubcategory(@RequestBody FoodSubcategory foodSubcategory);
	
	@PreAuthorize("hasRole('REGISTERED_USER')")
	@PostMapping(value = "/food-subcategory/create/all", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	List<FoodSubcategory> addAllFoodSubcategory(@RequestBody List<FoodSubcategory> foodSubcategories);
	
	@PreAuthorize("hasRole('REGISTERED_USER')")
	@GetMapping(value = "/food-subcategory/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	FoodSubcategory getFoodSubcategoryById(@PathVariable("id") String id);
	
	@PreAuthorize("hasRole('REGISTERED_USER')")
	@DeleteMapping(value = "/food-subcategory/{id}")
	void deleteFoodSubcategory(@PathVariable("id") String id);
}
