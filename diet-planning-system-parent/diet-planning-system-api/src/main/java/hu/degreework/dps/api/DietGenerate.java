package hu.degreework.dps.api;

import java.util.Objects;

public class DietGenerate {

	private TargetType dietGoal;
	private PriceCategoryType dietPrice;
	private MealPreferenceType dietMealType;
	private OilPreferencesType dietOil;
	private SugarPreferencesType dietSugar;
	private Integer dietPeriod;

	public TargetType getDietGoal() {
		return dietGoal;
	}

	public void setDietGoal(TargetType dietGoal) {
		this.dietGoal = dietGoal;
	}

	public PriceCategoryType getDietPrice() {
		return dietPrice;
	}

	public void setDietPrice(PriceCategoryType dietPrice) {
		this.dietPrice = dietPrice;
	}

	public MealPreferenceType getDietMealType() {
		return dietMealType;
	}

	public void setDietMealType(MealPreferenceType dietMealType) {
		this.dietMealType = dietMealType;
	}

	public OilPreferencesType getDietOil() {
		return dietOil;
	}

	public void setDietOil(OilPreferencesType dietOil) {
		this.dietOil = dietOil;
	}

	public SugarPreferencesType getDietSugar() {
		return dietSugar;
	}

	public void setDietSugar(SugarPreferencesType dietSugar) {
		this.dietSugar = dietSugar;
	}

	public Integer getDietPeriod() {
		return dietPeriod;
	}

	public void setDietPeriod(Integer dietPeriod) {
		this.dietPeriod = dietPeriod;
	}

	@Override
	public int hashCode() {
		return Objects.hash(dietGoal, dietMealType, dietOil, dietPeriod, dietPrice, dietSugar);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DietGenerate other = (DietGenerate) obj;
		return dietGoal == other.dietGoal && dietMealType == other.dietMealType && dietOil == other.dietOil
				&& Objects.equals(dietPeriod, other.dietPeriod) && dietPrice == other.dietPrice
				&& dietSugar == other.dietSugar;
	}
}
