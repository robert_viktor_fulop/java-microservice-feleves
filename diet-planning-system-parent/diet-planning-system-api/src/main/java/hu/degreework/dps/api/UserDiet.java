package hu.degreework.dps.api;

import java.util.Date;
import java.util.Objects;
import java.util.Set;

public class UserDiet {

	private String id;
	private Set<String> recipes;
	private String userTableId;
	private Date timestamp;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Set<String> getRecipes() {
		return recipes;
	}

	public void setRecipes(Set<String> recipes) {
		this.recipes = recipes;
	}

	public String getUserTableId() {
		return userTableId;
	}

	public void setUserTableId(String userTableId) {
		this.userTableId = userTableId;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, recipes, timestamp, userTableId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserDiet other = (UserDiet) obj;
		return Objects.equals(id, other.id) && Objects.equals(recipes, other.recipes)
				&& Objects.equals(timestamp, other.timestamp) && Objects.equals(userTableId, other.userTableId);
	}
}
