package hu.degreework.dps.api;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.Set;

public class Recipe {

	private String id;
	private String name;
	private String recipePreparation;
	private BigDecimal kVitamin;
	private BigDecimal dVitamin;
	private BigDecimal cVitamin;
	private BigDecimal bTwoVitamin;
	private BigDecimal bOneVitamin;
	private BigDecimal aVitamin;
	private BigDecimal magneseium;
	private BigDecimal calcium;
	private BigDecimal iron;
	private BigDecimal carbohydrate;
	private BigDecimal fat;
	private BigDecimal energy;
	private BigDecimal protein;
	private PartOfDayType partOfDay;
	private PriceCategoryType priceCategory;
	private OilPreferencesType oilPreferences;
	private SugarPreferencesType sugarPreferences;
	private Set<Ingredient> ingredients;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRecipePreparation() {
		return recipePreparation;
	}

	public void setRecipePreparation(String recipePreparation) {
		this.recipePreparation = recipePreparation;
	}

	public BigDecimal getkVitamin() {
		return kVitamin;
	}

	public void setkVitamin(BigDecimal kVitamin) {
		this.kVitamin = kVitamin;
	}

	public BigDecimal getdVitamin() {
		return dVitamin;
	}

	public void setdVitamin(BigDecimal dVitamin) {
		this.dVitamin = dVitamin;
	}

	public BigDecimal getcVitamin() {
		return cVitamin;
	}

	public void setcVitamin(BigDecimal cVitamin) {
		this.cVitamin = cVitamin;
	}

	public BigDecimal getbTwoVitamin() {
		return bTwoVitamin;
	}

	public void setbTwoVitamin(BigDecimal bTwoVitamin) {
		this.bTwoVitamin = bTwoVitamin;
	}

	public BigDecimal getbOneVitamin() {
		return bOneVitamin;
	}

	public void setbOneVitamin(BigDecimal bOneVitamin) {
		this.bOneVitamin = bOneVitamin;
	}

	public BigDecimal getaVitamin() {
		return aVitamin;
	}

	public void setaVitamin(BigDecimal aVitamin) {
		this.aVitamin = aVitamin;
	}

	public BigDecimal getMagneseium() {
		return magneseium;
	}

	public void setMagneseium(BigDecimal magneseium) {
		this.magneseium = magneseium;
	}

	public BigDecimal getCalcium() {
		return calcium;
	}

	public void setCalcium(BigDecimal calcium) {
		this.calcium = calcium;
	}

	public BigDecimal getIron() {
		return iron;
	}

	public void setIron(BigDecimal iron) {
		this.iron = iron;
	}

	public BigDecimal getCarbohydrate() {
		return carbohydrate;
	}

	public void setCarbohydrate(BigDecimal carbohydrate) {
		this.carbohydrate = carbohydrate;
	}

	public BigDecimal getFat() {
		return fat;
	}

	public void setFat(BigDecimal fat) {
		this.fat = fat;
	}

	public BigDecimal getEnergy() {
		return energy;
	}

	public void setEnergy(BigDecimal energy) {
		this.energy = energy;
	}

	public BigDecimal getProtein() {
		return protein;
	}

	public void setProtein(BigDecimal protein) {
		this.protein = protein;
	}

	public PartOfDayType getPartOfDay() {
		return partOfDay;
	}

	public void setPartOfDay(PartOfDayType partOfDay) {
		this.partOfDay = partOfDay;
	}

	public PriceCategoryType getPriceCategory() {
		return priceCategory;
	}

	public void setPriceCategory(PriceCategoryType priceCategory) {
		this.priceCategory = priceCategory;
	}

	public OilPreferencesType getOilPreferences() {
		return oilPreferences;
	}

	public void setOilPreferences(OilPreferencesType oilPreferences) {
		this.oilPreferences = oilPreferences;
	}

	public SugarPreferencesType getSugarPreferences() {
		return sugarPreferences;
	}

	public void setSugarPreferences(SugarPreferencesType sugarPreferences) {
		this.sugarPreferences = sugarPreferences;
	}

	public Set<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(Set<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	@Override
	public int hashCode() {
		return Objects.hash(aVitamin, bOneVitamin, bTwoVitamin, cVitamin, calcium, carbohydrate, dVitamin, energy, fat,
				id, ingredients, iron, kVitamin, magneseium, name, oilPreferences, partOfDay, priceCategory, protein,
				recipePreparation, sugarPreferences);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Recipe other = (Recipe) obj;
		return Objects.equals(aVitamin, other.aVitamin) && Objects.equals(bOneVitamin, other.bOneVitamin)
				&& Objects.equals(bTwoVitamin, other.bTwoVitamin) && Objects.equals(cVitamin, other.cVitamin)
				&& Objects.equals(calcium, other.calcium) && Objects.equals(carbohydrate, other.carbohydrate)
				&& Objects.equals(dVitamin, other.dVitamin) && Objects.equals(energy, other.energy)
				&& Objects.equals(fat, other.fat) && Objects.equals(id, other.id)
				&& Objects.equals(ingredients, other.ingredients) && Objects.equals(iron, other.iron)
				&& Objects.equals(kVitamin, other.kVitamin) && Objects.equals(magneseium, other.magneseium)
				&& Objects.equals(name, other.name) && oilPreferences == other.oilPreferences
				&& partOfDay == other.partOfDay && priceCategory == other.priceCategory
				&& Objects.equals(protein, other.protein) && Objects.equals(recipePreparation, other.recipePreparation)
				&& sugarPreferences == other.sugarPreferences;
	}

}
