package hu.degreework.dps.persistence;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "daily_diet")
public class DailyDietEntity {

	@Id
	private String id;
	private String breakfastId;
	private String breakfastName;
	private BigDecimal breakfastCalorie;
	private String brunchId;
	private String brunchName;
	private BigDecimal brunchCalorie;
	private String lunchSoupId;
	private String lunchSoupName;
	private BigDecimal lunchSoupCalorie;
	private String lunchMainDishId;
	private String lunchMainDishName;
	private BigDecimal lunchMainDishCalorie;
	private String snackId;
	private String snackName;
	private BigDecimal snackCalorie;
	private String dinnerId;
	private String dinnerName;
	private BigDecimal dinnerCalorie;
	private Double dailyCalorieIntake;
	private Integer number;
	@ManyToOne
	@JoinColumn(name = "dietPlanning_id")
	private DietPlanningEntity dietPlanning;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBreakfastId() {
		return breakfastId;
	}

	public void setBreakfastId(String breakfastId) {
		this.breakfastId = breakfastId;
	}

	public String getBreakfastName() {
		return breakfastName;
	}

	public void setBreakfastName(String breakfastName) {
		this.breakfastName = breakfastName;
	}

	public BigDecimal getBreakfastCalorie() {
		return breakfastCalorie;
	}

	public void setBreakfastCalorie(BigDecimal breakfastCalorie) {
		this.breakfastCalorie = breakfastCalorie;
	}

	public String getBrunchId() {
		return brunchId;
	}

	public void setBrunchId(String brunchId) {
		this.brunchId = brunchId;
	}

	public String getBrunchName() {
		return brunchName;
	}

	public void setBrunchName(String brunchName) {
		this.brunchName = brunchName;
	}

	public BigDecimal getBrunchCalorie() {
		return brunchCalorie;
	}

	public void setBrunchCalorie(BigDecimal brunchCalorie) {
		this.brunchCalorie = brunchCalorie;
	}

	public String getLunchSoupId() {
		return lunchSoupId;
	}

	public void setLunchSoupId(String lunchSoupId) {
		this.lunchSoupId = lunchSoupId;
	}

	public String getLunchSoupName() {
		return lunchSoupName;
	}

	public void setLunchSoupName(String lunchSoupName) {
		this.lunchSoupName = lunchSoupName;
	}

	public BigDecimal getLunchSoupCalorie() {
		return lunchSoupCalorie;
	}

	public void setLunchSoupCalorie(BigDecimal lunchSoupCalorie) {
		this.lunchSoupCalorie = lunchSoupCalorie;
	}

	public String getLunchMainDishId() {
		return lunchMainDishId;
	}

	public void setLunchMainDishId(String lunchMainDishId) {
		this.lunchMainDishId = lunchMainDishId;
	}

	public String getLunchMainDishName() {
		return lunchMainDishName;
	}

	public void setLunchMainDishName(String lunchMainDishName) {
		this.lunchMainDishName = lunchMainDishName;
	}

	public BigDecimal getLunchMainDishCalorie() {
		return lunchMainDishCalorie;
	}

	public void setLunchMainDishCalorie(BigDecimal lunchMainDishCalorie) {
		this.lunchMainDishCalorie = lunchMainDishCalorie;
	}

	public String getSnackId() {
		return snackId;
	}

	public void setSnackId(String snackId) {
		this.snackId = snackId;
	}

	public String getSnackName() {
		return snackName;
	}

	public void setSnackName(String snackName) {
		this.snackName = snackName;
	}

	public BigDecimal getSnackCalorie() {
		return snackCalorie;
	}

	public void setSnackCalorie(BigDecimal snackCalorie) {
		this.snackCalorie = snackCalorie;
	}

	public String getDinnerId() {
		return dinnerId;
	}

	public void setDinnerId(String dinnerId) {
		this.dinnerId = dinnerId;
	}

	public String getDinnerName() {
		return dinnerName;
	}

	public void setDinnerName(String dinnerName) {
		this.dinnerName = dinnerName;
	}

	public BigDecimal getDinnerCalorie() {
		return dinnerCalorie;
	}

	public void setDinnerCalorie(BigDecimal dinnerCalorie) {
		this.dinnerCalorie = dinnerCalorie;
	}

	public Double getDailyCalorieIntake() {
		return dailyCalorieIntake;
	}

	public void setDailyCalorieIntake(Double dailyCalorieIntake) {
		this.dailyCalorieIntake = dailyCalorieIntake;
	}

	public DietPlanningEntity getDietPlanning() {
		return dietPlanning;
	}

	public void setDietPlanning(DietPlanningEntity dietPlanning) {
		this.dietPlanning = dietPlanning;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

}
