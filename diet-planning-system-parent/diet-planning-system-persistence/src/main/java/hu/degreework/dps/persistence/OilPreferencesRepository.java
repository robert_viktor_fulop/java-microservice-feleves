package hu.degreework.dps.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

public interface OilPreferencesRepository extends JpaRepository<OilPreferencesEntity,String>{

}
