package hu.degreework.dps.persistence;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "user_keycloak")
public class UserEntity {

	@Id
	private String id;
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date")
	private Date registration;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "gender_id")
	private GenderEntity gender;
	private Date birthday;
	private Boolean genderAndBirthdayCompleted;
	@OneToMany(mappedBy = "user")
	private Set<DietPlanningEntity> usersDiets;
	@OneToMany(mappedBy = "user")
	private Set<DietCalendarEntity> dietCalendars;
	@OneToMany(mappedBy = "user")
	private Set<UserAnthropometersEntity> userAnthropometers;
	@OneToMany(mappedBy = "user")
	private Set<MineralStatisticsEntity> mineralStatistics;
	@OneToMany(mappedBy = "user")
	private Set<VitaminStatisticsEntity> vitaminStatistics;
	@OneToMany(mappedBy = "user")
	private Set<MacronutrientStatisticsEntity> macronutrientStatistics;
	@OneToMany(mappedBy = "user")
	private Set<AnthropometerStatisticsEntity> anthropometerStatistics;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getRegistration() {
		return registration;
	}

	public void setRegistration(Date registration) {
		this.registration = registration;
	}

	public GenderEntity getGender() {
		return gender;
	}

	public void setGender(GenderEntity gender) {
		this.gender = gender;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Boolean getGenderAndBirthdayCompleted() {
		return genderAndBirthdayCompleted;
	}

	public void setGenderAndBirthdayCompleted(Boolean genderAndBirthdayCompleted) {
		this.genderAndBirthdayCompleted = genderAndBirthdayCompleted;
	}

	public Set<DietPlanningEntity> getUsersDiets() {
		return usersDiets;
	}

	public void setUsersDiets(Set<DietPlanningEntity> usersDiets) {
		this.usersDiets = usersDiets;
	}

	public Set<UserAnthropometersEntity> getUserAnthropometers() {
		return userAnthropometers;
	}

	public void setUserAnthropometers(Set<UserAnthropometersEntity> userAnthropometers) {
		this.userAnthropometers = userAnthropometers;
	}

	public Set<MineralStatisticsEntity> getMineralStatistics() {
		return mineralStatistics;
	}

	public void setMineralStatistics(Set<MineralStatisticsEntity> mineralStatistics) {
		this.mineralStatistics = mineralStatistics;
	}

	public Set<VitaminStatisticsEntity> getVitaminStatistics() {
		return vitaminStatistics;
	}

	public void setVitaminStatistics(Set<VitaminStatisticsEntity> vitaminStatistics) {
		this.vitaminStatistics = vitaminStatistics;
	}

	public Set<MacronutrientStatisticsEntity> getMacronutrientStatistics() {
		return macronutrientStatistics;
	}

	public void setMacronutrientStatistics(Set<MacronutrientStatisticsEntity> macronutrientStatistics) {
		this.macronutrientStatistics = macronutrientStatistics;
	}

	public Set<AnthropometerStatisticsEntity> getAnthropometerStatistics() {
		return anthropometerStatistics;
	}

	public void setAnthropometerStatistics(Set<AnthropometerStatisticsEntity> anthropometerStatistics) {
		this.anthropometerStatistics = anthropometerStatistics;
	}

}
