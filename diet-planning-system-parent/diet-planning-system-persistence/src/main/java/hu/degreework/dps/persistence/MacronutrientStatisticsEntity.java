package hu.degreework.dps.persistence;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import hu.degreework.dps.api.TimeIntervalType;

@Entity
@Table(name = "macronutrient_statistics")
public class MacronutrientStatisticsEntity {

	@Id
	private String id;
	private BigDecimal energy;
	private BigDecimal protein;
	private BigDecimal fat;
	private BigDecimal carbohydrate;
	@Enumerated(EnumType.STRING)
	private TimeIntervalType timeInterval;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id")
	private UserEntity user;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BigDecimal getEnergy() {
		return energy;
	}

	public void setEnergy(BigDecimal energy) {
		this.energy = energy;
	}

	public BigDecimal getProtein() {
		return protein;
	}

	public void setProtein(BigDecimal protein) {
		this.protein = protein;
	}

	public BigDecimal getFat() {
		return fat;
	}

	public void setFat(BigDecimal fat) {
		this.fat = fat;
	}

	public BigDecimal getCarbohydrate() {
		return carbohydrate;
	}

	public void setCarbohydrate(BigDecimal carbohydrate) {
		this.carbohydrate = carbohydrate;
	}

	public TimeIntervalType getTimeInterval() {
		return timeInterval;
	}

	public void setTimeInterval(TimeIntervalType timeInterval) {
		this.timeInterval = timeInterval;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}
}
