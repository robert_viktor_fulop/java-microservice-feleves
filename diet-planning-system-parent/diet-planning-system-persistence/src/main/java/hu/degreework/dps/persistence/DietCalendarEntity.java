package hu.degreework.dps.persistence;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "diet_calendar")
public class DietCalendarEntity {

	@Id
	private String id;
	private String dailyDietId;
	private String dietPlanningId;
	private String breakfastId;
	private String breakfastName;
	private String brunchId;
	private String brunchName;
	private String lunchSoupId;
	private String lunchSoupName;
	private String lunchMainDishId;
	private String lunchMainDishName;
	private String snackId;
	private String snackName;
	private String dinnerId;
	private String dinnerName;
	private LocalDate consumption;
	
	@ManyToOne
	@JoinColumn(name = "user_id")
	private UserEntity user;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDailyDietId() {
		return dailyDietId;
	}

	public void setDailyDietId(String dailyDietId) {
		this.dailyDietId = dailyDietId;
	}

	public String getDietPlanningId() {
		return dietPlanningId;
	}

	public void setDietPlanningId(String dietPlanningId) {
		this.dietPlanningId = dietPlanningId;
	}

	public String getBreakfastId() {
		return breakfastId;
	}

	public void setBreakfastId(String breakfastId) {
		this.breakfastId = breakfastId;
	}

	public String getBreakfastName() {
		return breakfastName;
	}

	public void setBreakfastName(String breakfastName) {
		this.breakfastName = breakfastName;
	}

	public String getBrunchId() {
		return brunchId;
	}

	public void setBrunchId(String brunchId) {
		this.brunchId = brunchId;
	}

	public String getBrunchName() {
		return brunchName;
	}

	public void setBrunchName(String brunchName) {
		this.brunchName = brunchName;
	}

	public String getLunchSoupId() {
		return lunchSoupId;
	}

	public void setLunchSoupId(String lunchSoupId) {
		this.lunchSoupId = lunchSoupId;
	}

	public String getLunchSoupName() {
		return lunchSoupName;
	}

	public void setLunchSoupName(String lunchSoupName) {
		this.lunchSoupName = lunchSoupName;
	}

	public String getLunchMainDishId() {
		return lunchMainDishId;
	}

	public void setLunchMainDishId(String lunchMainDishId) {
		this.lunchMainDishId = lunchMainDishId;
	}

	public String getLunchMainDishName() {
		return lunchMainDishName;
	}

	public void setLunchMainDishName(String lunchMainDishName) {
		this.lunchMainDishName = lunchMainDishName;
	}

	public String getSnackId() {
		return snackId;
	}

	public void setSnackId(String snackId) {
		this.snackId = snackId;
	}

	public String getSnackName() {
		return snackName;
	}

	public void setSnackName(String snackName) {
		this.snackName = snackName;
	}

	public String getDinnerId() {
		return dinnerId;
	}

	public void setDinnerId(String dinnerId) {
		this.dinnerId = dinnerId;
	}

	public String getDinnerName() {
		return dinnerName;
	}

	public void setDinnerName(String dinnerName) {
		this.dinnerName = dinnerName;
	}

	public LocalDate getConsumption() {
		return consumption;
	}

	public void setConsumption(LocalDate consumption) {
		this.consumption = consumption;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

}
