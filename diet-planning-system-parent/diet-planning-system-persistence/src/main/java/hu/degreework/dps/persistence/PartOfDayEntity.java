package hu.degreework.dps.persistence;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import hu.degreework.dps.api.PartOfDayType;

@Entity
@Table(name = "part_of_day")
public class PartOfDayEntity {

	@Id
	private String id;
	@Enumerated(EnumType.STRING)
	private PartOfDayType name;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "partOfDay")
	private Set<RecipeEntity> recipes;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public PartOfDayType getName() {
		return name;
	}

	public void setName(PartOfDayType name) {
		this.name = name;
	}

	public Set<RecipeEntity> getRecipes() {
		return recipes;
	}

	public void setRecipes(Set<RecipeEntity> recipes) {
		this.recipes = recipes;
	}
}
