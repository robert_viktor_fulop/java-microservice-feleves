package hu.degreework.dps.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserAnthropometersRepository extends JpaRepository<UserAnthropometersEntity, String> {
}
