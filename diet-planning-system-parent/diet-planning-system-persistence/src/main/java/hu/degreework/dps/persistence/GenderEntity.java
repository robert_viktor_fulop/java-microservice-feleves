package hu.degreework.dps.persistence;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import hu.degreework.dps.api.GenderType;

@Entity
@Table(name = "gender")
public class GenderEntity {

	@Id
	private String id;
	@Enumerated(EnumType.STRING)
	private GenderType genderType;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "gender")
	private Set<UserEntity> users;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public GenderType getGenderType() {
		return genderType;
	}

	public void setGenderType(GenderType genderType) {
		this.genderType = genderType;
	}

	public Set<UserEntity> getUsers() {
		return users;
	}

	public void setUsers(Set<UserEntity> users) {
		this.users = users;
	}

}
