package hu.degreework.dps.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FoodSubcategoryRepository extends JpaRepository<FoodSubcategoryEntity, String>{

}
