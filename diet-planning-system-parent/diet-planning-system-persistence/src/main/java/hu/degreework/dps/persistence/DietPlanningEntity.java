package hu.degreework.dps.persistence;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "diet_planning")
public class DietPlanningEntity {

	@Id
	private String id;
	@ManyToOne
	@JoinColumn(name = "user_id")
	private UserEntity user;
	@OneToMany(mappedBy = "dietPlanning")
	private Set<DailyDietEntity> dailyDites;
	private Boolean isDeletable;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public Set<DailyDietEntity> getDailyDites() {
		return dailyDites;
	}

	public void setDailyDites(Set<DailyDietEntity> dailyDites) {
		this.dailyDites = dailyDites;
	}

	public Boolean getIsDeletable() {
		return isDeletable;
	}

	public void setIsDeletable(Boolean isDeletable) {
		this.isDeletable = isDeletable;
	}

}
