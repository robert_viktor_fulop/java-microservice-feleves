package hu.degreework.dps.persistence;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="food_subcategory")
public class FoodSubcategoryEntity {

	@Id
	@Column(name="subcategory_id")
	private String id;
	@Column(name="subcategory", nullable = false)
	private String subcategory;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "subcategory")
	private Set<FoodMaterialEntity> foods = new HashSet<>();
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSubcategory() {
		return subcategory;
	}
	public void setSubcategory(String subcategory) {
		this.subcategory = subcategory;
	}
	public Set<FoodMaterialEntity> getFoods() {
		return foods;
	}
	public void setFoods(Set<FoodMaterialEntity> foods) {
		this.foods = foods;
	}
}
