package hu.degreework.dps.persistence;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "food_material")
public class FoodMaterialEntity {

	@Id
	private String id;
	@Column(name = "name", nullable = false)
	private String name;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "category_id")
	private FoodCategroyEntity category;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "subcategory_id")
	private FoodSubcategoryEntity subcategory;
	private BigDecimal energyKcal;
	private BigDecimal proteinGram;
	private BigDecimal fatGram;
	private BigDecimal carbohydrateGram;
	private String gi;
	private BigDecimal aVitamin;
	private BigDecimal bOneVitamin;
	private BigDecimal bTwoVitamin;
	private BigDecimal cVitamin;
	private BigDecimal dVitamin;
	private BigDecimal kVitamin;
	private BigDecimal calcium;
	private BigDecimal iron;
	private BigDecimal magnesium;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "foodMaterials")
	private Set<IngredientEntity> ingredients = new HashSet<>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public FoodCategroyEntity getCategory() {
		return category;
	}

	public void setCategory(FoodCategroyEntity category) {
		this.category = category;
	}

	public FoodSubcategoryEntity getSubcategory() {
		return subcategory;
	}

	public void setSubcategory(FoodSubcategoryEntity subcategory) {
		this.subcategory = subcategory;
	}

	public BigDecimal getEnergyKcal() {
		return energyKcal;
	}

	public void setEnergyKcal(BigDecimal energyKcal) {
		this.energyKcal = energyKcal;
	}

	public BigDecimal getProteinGram() {
		return proteinGram;
	}

	public void setProteinGram(BigDecimal proteinGram) {
		this.proteinGram = proteinGram;
	}

	public BigDecimal getFatGram() {
		return fatGram;
	}

	public void setFatGram(BigDecimal fatGram) {
		this.fatGram = fatGram;
	}

	public BigDecimal getCarbohydrateGram() {
		return carbohydrateGram;
	}

	public void setCarbohydrateGram(BigDecimal carbohydrateGram) {
		this.carbohydrateGram = carbohydrateGram;
	}

	public String getGi() {
		return gi;
	}

	public void setGi(String gi) {
		this.gi = gi;
	}

	public BigDecimal getaVitamin() {
		return aVitamin;
	}

	public void setaVitamin(BigDecimal aVitamin) {
		this.aVitamin = aVitamin;
	}

	public BigDecimal getbOneVitamin() {
		return bOneVitamin;
	}

	public void setbOneVitamin(BigDecimal bOneVitamin) {
		this.bOneVitamin = bOneVitamin;
	}

	public BigDecimal getbTwoVitamin() {
		return bTwoVitamin;
	}

	public void setbTwoVitamin(BigDecimal bTwoVitamin) {
		this.bTwoVitamin = bTwoVitamin;
	}

	public BigDecimal getcVitamin() {
		return cVitamin;
	}

	public void setcVitamin(BigDecimal cVitamin) {
		this.cVitamin = cVitamin;
	}

	public BigDecimal getdVitamin() {
		return dVitamin;
	}

	public void setdVitamin(BigDecimal dVitamin) {
		this.dVitamin = dVitamin;
	}

	public BigDecimal getkVitamin() {
		return kVitamin;
	}

	public void setkVitamin(BigDecimal kVitamin) {
		this.kVitamin = kVitamin;
	}

	public BigDecimal getCalcium() {
		return calcium;
	}

	public void setCalcium(BigDecimal calcium) {
		this.calcium = calcium;
	}

	public BigDecimal getIron() {
		return iron;
	}

	public void setIron(BigDecimal iron) {
		this.iron = iron;
	}

	public BigDecimal getMagnesium() {
		return magnesium;
	}

	public void setMagnesium(BigDecimal magnesium) {
		this.magnesium = magnesium;
	}

	public Set<IngredientEntity> getIngredients() {
		return ingredients;
	}

	public void setIngredients(Set<IngredientEntity> ingredients) {
		this.ingredients = ingredients;
	}

}
