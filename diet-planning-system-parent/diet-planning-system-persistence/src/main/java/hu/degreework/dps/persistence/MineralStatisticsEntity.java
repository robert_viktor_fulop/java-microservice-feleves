package hu.degreework.dps.persistence;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import hu.degreework.dps.api.TimeIntervalType;

@Entity
@Table(name = "mineral_statistics")
public class MineralStatisticsEntity {

	@Id
	private String id;
	private BigDecimal iron;
	private BigDecimal calcium;
	private BigDecimal magnesium;
	@Enumerated(EnumType.STRING)
	private TimeIntervalType timeInterval;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id")
	private UserEntity user;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BigDecimal getIron() {
		return iron;
	}

	public void setIron(BigDecimal iron) {
		this.iron = iron;
	}

	public BigDecimal getCalcium() {
		return calcium;
	}

	public void setCalcium(BigDecimal calcium) {
		this.calcium = calcium;
	}

	public BigDecimal getMagnesium() {
		return magnesium;
	}

	public void setMagnesium(BigDecimal magnesium) {
		this.magnesium = magnesium;
	}

	public TimeIntervalType getTimeInterval() {
		return timeInterval;
	}

	public void setTimeInterval(TimeIntervalType timeInterval) {
		this.timeInterval = timeInterval;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}
}
