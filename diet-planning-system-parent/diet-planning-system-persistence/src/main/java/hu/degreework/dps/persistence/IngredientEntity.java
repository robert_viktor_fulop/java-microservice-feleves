package hu.degreework.dps.persistence;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="ingredient")
public class IngredientEntity {

	@Id
	private String id;
	private BigDecimal quantity;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="unit_of_measure_id")
	private UnitOfMeasureEntity unitOfMeasures;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="food_material_id")
	private FoodMaterialEntity foodMaterials;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="recipe_id")
	private RecipeEntity recipes;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public UnitOfMeasureEntity getUnitOfMeasures() {
		return unitOfMeasures;
	}

	public void setUnitOfMeasures(UnitOfMeasureEntity unitOfMeasures) {
		this.unitOfMeasures = unitOfMeasures;
	}

	public FoodMaterialEntity getFoodMaterials() {
		return foodMaterials;
	}

	public void setFoodMaterials(FoodMaterialEntity foodMaterials) {
		this.foodMaterials = foodMaterials;
	}

	public RecipeEntity getRecipes() {
		return recipes;
	}

	public void setRecipes(RecipeEntity recipes) {
		this.recipes = recipes;
	}
		
}
