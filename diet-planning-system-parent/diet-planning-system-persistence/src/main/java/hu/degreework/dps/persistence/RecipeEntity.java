package hu.degreework.dps.persistence;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "recipe")
public class RecipeEntity {

	@Id
	private String id;
	private String name;
	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String recipePreparation;
	private String userKeyckloakId;
	private BigDecimal kVitamin;
	private BigDecimal dVitamin;
	private BigDecimal cVitamin;
	private BigDecimal bTwoVitamin;
	private BigDecimal bOneVitamin;
	private BigDecimal aVitamin;
	private BigDecimal magneseium;
	private BigDecimal calcium;
	private BigDecimal iron;
	private BigDecimal carbohydrate;
	private BigDecimal fat;
	private BigDecimal energy;
	private BigDecimal protein;
	private Boolean soup;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "part_of_day_id")
	private PartOfDayEntity partOfDay;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "price_category_id")
	private PriceCategoryEntity priceCategory;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "oil_preferences_id")
	private OilPreferencesEntity oilPreferences;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "sugar_preferences_id")
	private SugarPreferencesEntity sugarPreferences;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "recipes")
	private Set<IngredientEntity> ingredients;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRecipePreparation() {
		return recipePreparation;
	}

	public void setRecipePreparation(String recipePreparation) {
		this.recipePreparation = recipePreparation;
	}

	public String getUserKeyckloakId() {
		return userKeyckloakId;
	}

	public void setUserKeyckloakId(String userKeyckloakId) {
		this.userKeyckloakId = userKeyckloakId;
	}

	public BigDecimal getkVitamin() {
		return kVitamin;
	}

	public void setkVitamin(BigDecimal kVitamin) {
		this.kVitamin = kVitamin;
	}

	public BigDecimal getdVitamin() {
		return dVitamin;
	}

	public void setdVitamin(BigDecimal dVitamin) {
		this.dVitamin = dVitamin;
	}

	public BigDecimal getcVitamin() {
		return cVitamin;
	}

	public void setcVitamin(BigDecimal cVitamin) {
		this.cVitamin = cVitamin;
	}

	public BigDecimal getbTwoVitamin() {
		return bTwoVitamin;
	}

	public void setbTwoVitamin(BigDecimal bTwoVitamin) {
		this.bTwoVitamin = bTwoVitamin;
	}

	public BigDecimal getbOneVitamin() {
		return bOneVitamin;
	}

	public void setbOneVitamin(BigDecimal bOneVitamin) {
		this.bOneVitamin = bOneVitamin;
	}

	public BigDecimal getaVitamin() {
		return aVitamin;
	}

	public void setaVitamin(BigDecimal aVitamin) {
		this.aVitamin = aVitamin;
	}

	public BigDecimal getMagneseium() {
		return magneseium;
	}

	public void setMagneseium(BigDecimal magneseium) {
		this.magneseium = magneseium;
	}

	public BigDecimal getCalcium() {
		return calcium;
	}

	public void setCalcium(BigDecimal calcium) {
		this.calcium = calcium;
	}

	public BigDecimal getIron() {
		return iron;
	}

	public void setIron(BigDecimal iron) {
		this.iron = iron;
	}

	public BigDecimal getCarbohydrate() {
		return carbohydrate;
	}

	public void setCarbohydrate(BigDecimal carbohydrate) {
		this.carbohydrate = carbohydrate;
	}

	public BigDecimal getFat() {
		return fat;
	}

	public void setFat(BigDecimal fat) {
		this.fat = fat;
	}

	public BigDecimal getEnergy() {
		return energy;
	}

	public void setEnergy(BigDecimal energy) {
		this.energy = energy;
	}

	public BigDecimal getProtein() {
		return protein;
	}

	public void setProtein(BigDecimal protein) {
		this.protein = protein;
	}

	public Boolean getSoup() {
		return soup;
	}

	public void setSoup(Boolean soup) {
		this.soup = soup;
	}

	public PartOfDayEntity getPartOfDay() {
		return partOfDay;
	}

	public void setPartOfDay(PartOfDayEntity partOfDay) {
		this.partOfDay = partOfDay;
	}

	public PriceCategoryEntity getPriceCategory() {
		return priceCategory;
	}

	public void setPriceCategory(PriceCategoryEntity priceCategory) {
		this.priceCategory = priceCategory;
	}

	public OilPreferencesEntity getOilPreferences() {
		return oilPreferences;
	}

	public void setOilPreferences(OilPreferencesEntity oilPreferences) {
		this.oilPreferences = oilPreferences;
	}

	public SugarPreferencesEntity getSugarPreferences() {
		return sugarPreferences;
	}

	public void setSugarPreferences(SugarPreferencesEntity sugarPreferences) {
		this.sugarPreferences = sugarPreferences;
	}

	public Set<IngredientEntity> getIngredients() {
		return ingredients;
	}

	public void setIngredients(Set<IngredientEntity> ingredients) {
		this.ingredients = ingredients;
	}
}
