package hu.degreework.dps.persistence;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import hu.degreework.dps.api.TimeIntervalType;

@Entity
@Table(name = "anthropometer_statistics")
public class AnthropometerStatisticsEntity {

	@Id
	private String id;
	private BigDecimal bmi;
	private BigDecimal bmr;
	private BigDecimal bfp;
	private BigDecimal bfw;
	private BigDecimal lbm;
	@Enumerated(EnumType.STRING)
	private TimeIntervalType timeInterval;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id")
	private UserEntity user;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BigDecimal getBmi() {
		return bmi;
	}

	public void setBmi(BigDecimal bmi) {
		this.bmi = bmi;
	}

	public BigDecimal getBmr() {
		return bmr;
	}

	public void setBmr(BigDecimal bmr) {
		this.bmr = bmr;
	}

	public BigDecimal getBfp() {
		return bfp;
	}

	public void setBfp(BigDecimal bfp) {
		this.bfp = bfp;
	}

	public BigDecimal getBfw() {
		return bfw;
	}

	public void setBfw(BigDecimal bfw) {
		this.bfw = bfw;
	}

	public BigDecimal getLbm() {
		return lbm;
	}

	public void setLbm(BigDecimal lbm) {
		this.lbm = lbm;
	}

	public TimeIntervalType getTimeInterval() {
		return timeInterval;
	}

	public void setTimeInterval(TimeIntervalType timeInterval) {
		this.timeInterval = timeInterval;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}
}
