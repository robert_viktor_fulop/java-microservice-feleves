package hu.degreework.dps.persistence;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="food_category")
public class FoodCategroyEntity {

	@Id
	@Column(name="category_id")
	private String id;
	@Column(name="category", nullable = false)
	private String category;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "category")
	private Set<FoodMaterialEntity> foods = new HashSet<>();
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Set<FoodMaterialEntity> getFoods() {
		return foods;
	}
	public void setFoods(Set<FoodMaterialEntity> foods) {
		this.foods = foods;
	}

}
