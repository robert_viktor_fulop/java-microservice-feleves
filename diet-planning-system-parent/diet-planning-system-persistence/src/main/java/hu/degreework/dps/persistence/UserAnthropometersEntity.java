package hu.degreework.dps.persistence;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.CreationTimestamp;
import hu.degreework.dps.api.KatchMcArdleType;

@Entity
@Table(name = "user_anthropometers")
public class UserAnthropometersEntity {

	@Id
	private String id;
	@Enumerated(EnumType.STRING)
	private KatchMcArdleType katchMcArdle;
	private Integer age;
	private String activity;
	private Double weight;
	private Double height;
	private Double waist;
	private Double forearm;
	private Double wrist;
	private Double hip;
	private Double bmr;
	private Double bfp;
	private Double bfw;
	private Double lbmFemale;
	private Double lbmMale;
	private Double bmi;
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date")
	private Date timestamp;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id")
	private UserEntity user;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public KatchMcArdleType getKatchMcArdle() {
		return katchMcArdle;
	}

	public void setKatchMcArdle(KatchMcArdleType katchMcArdle) {
		this.katchMcArdle = katchMcArdle;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getHeight() {
		return height;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	public Double getWaist() {
		return waist;
	}

	public void setWaist(Double waist) {
		this.waist = waist;
	}

	public Double getForearm() {
		return forearm;
	}

	public void setForearm(Double forearm) {
		this.forearm = forearm;
	}

	public Double getWrist() {
		return wrist;
	}

	public void setWrist(Double wrist) {
		this.wrist = wrist;
	}

	public Double getHip() {
		return hip;
	}

	public void setHip(Double hip) {
		this.hip = hip;
	}

	public Double getBmr() {
		return bmr;
	}

	public void setBmr(Double bmr) {
		this.bmr = bmr;
	}

	public Double getBfp() {
		return bfp;
	}

	public void setBfp(Double bfp) {
		this.bfp = bfp;
	}

	public Double getBfw() {
		return bfw;
	}

	public void setBfw(Double bfw) {
		this.bfw = bfw;
	}

	public Double getLbmFemale() {
		return lbmFemale;
	}

	public void setLbmFemale(Double lbmFemale) {
		this.lbmFemale = lbmFemale;
	}

	public Double getLbmMale() {
		return lbmMale;
	}

	public void setLbmMale(Double lbmMale) {
		this.lbmMale = lbmMale;
	}

	public Double getBmi() {
		return bmi;
	}

	public void setBmi(Double bmi) {
		this.bmi = bmi;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}
}
