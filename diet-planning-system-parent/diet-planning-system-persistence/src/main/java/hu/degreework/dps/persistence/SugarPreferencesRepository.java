package hu.degreework.dps.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SugarPreferencesRepository extends JpaRepository<SugarPreferencesEntity,String> {

}
