package hu.degreework.dps.persistence;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="unit_of_measure")
public class UnitOfMeasureEntity {

	@Id
	private String id;
	private String name;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "unitOfMeasures")
	private Set<IngredientEntity> ingredients = new HashSet<>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<IngredientEntity> getIngredients() {
		return ingredients;
	}

	public void setIngredients(Set<IngredientEntity> ingredients) {
		this.ingredients = ingredients;
	}
}
