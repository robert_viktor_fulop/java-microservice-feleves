package hu.degreework.dps.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DailyDietRepository extends JpaRepository<DailyDietEntity,String>{

}
