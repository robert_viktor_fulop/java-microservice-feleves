package hu.degreework.dps.persistence;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import hu.degreework.dps.api.TimeIntervalType;

@Entity
@Table(name = "vitamin_statistics")
public class VitaminStatisticsEntity {

	@Id
	private String id;
	private BigDecimal aVitamin;
	private BigDecimal kVitamin;
	private BigDecimal dVitamin;
	private BigDecimal cVitamin;
	private BigDecimal bOneVitamin;
	private BigDecimal bTwoVitamin;
	@Enumerated(EnumType.STRING)
	private TimeIntervalType timeInterval;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id")
	private UserEntity user;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BigDecimal getaVitamin() {
		return aVitamin;
	}

	public void setaVitamin(BigDecimal aVitamin) {
		this.aVitamin = aVitamin;
	}

	public BigDecimal getkVitamin() {
		return kVitamin;
	}

	public void setkVitamin(BigDecimal kVitamin) {
		this.kVitamin = kVitamin;
	}

	public BigDecimal getdVitamin() {
		return dVitamin;
	}

	public void setdVitamin(BigDecimal dVitamin) {
		this.dVitamin = dVitamin;
	}

	public BigDecimal getcVitamin() {
		return cVitamin;
	}

	public void setcVitamin(BigDecimal cVitamin) {
		this.cVitamin = cVitamin;
	}

	public BigDecimal getbOneVitamin() {
		return bOneVitamin;
	}

	public void setbOneVitamin(BigDecimal bOneVitamin) {
		this.bOneVitamin = bOneVitamin;
	}

	public BigDecimal getbTwoVitamin() {
		return bTwoVitamin;
	}

	public void setbTwoVitamin(BigDecimal bTwoVitamin) {
		this.bTwoVitamin = bTwoVitamin;
	}

	public TimeIntervalType getTimeInterval() {
		return timeInterval;
	}

	public void setTimeInterval(TimeIntervalType timeInterval) {
		this.timeInterval = timeInterval;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}
}
