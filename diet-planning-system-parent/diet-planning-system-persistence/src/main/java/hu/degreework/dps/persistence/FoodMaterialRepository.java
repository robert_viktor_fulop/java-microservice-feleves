package hu.degreework.dps.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FoodMaterialRepository extends JpaRepository<FoodMaterialEntity, String>{

}
