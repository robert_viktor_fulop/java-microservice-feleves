package hu.degreework.dps.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DietPlanningRepository extends JpaRepository<DietPlanningEntity,String>{

}
