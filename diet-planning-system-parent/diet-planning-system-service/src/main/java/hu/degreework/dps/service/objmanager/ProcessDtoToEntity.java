package hu.degreework.dps.service.objmanager;


import java.time.LocalDate;
import java.util.Date;
import java.util.UUID;
import org.springframework.stereotype.Component;

import hu.degreework.dps.api.DietCalendar;
import hu.degreework.dps.api.FoodCategory;
import hu.degreework.dps.api.FoodSubcategory;
import hu.degreework.dps.api.Gender;
import hu.degreework.dps.api.GeneratedDiets;
import hu.degreework.dps.api.PlannedDiet;
import hu.degreework.dps.api.UserAnthropometers;
import hu.degreework.dps.persistence.DailyDietEntity;
import hu.degreework.dps.persistence.DietCalendarEntity;
import hu.degreework.dps.persistence.FoodCategroyEntity;
import hu.degreework.dps.persistence.FoodSubcategoryEntity;
import hu.degreework.dps.persistence.GenderEntity;
import hu.degreework.dps.persistence.RecipeEntity;
import hu.degreework.dps.persistence.UserAnthropometersEntity;
import hu.degreework.dps.persistence.UserEntity;

@Component
public class ProcessDtoToEntity {

	
	public FoodSubcategoryEntity foodSubCatprocessDtoToEntity(FoodSubcategory apiObject) {
		var entity = new FoodSubcategoryEntity();
		entity.setId(UUID.randomUUID().toString());
		entity.setSubcategory(apiObject.getName());
		return entity;
	}
	
	public FoodCategroyEntity foodCatprocessDtoToEntity(FoodCategory apiObject) {
		var entity = new FoodCategroyEntity();
		entity.setId(UUID.randomUUID().toString());
		entity.setCategory(apiObject.getName());
		return entity;
	}
		
	public UserAnthropometersEntity userAnthropometersDtoToEntity(UserAnthropometers apiObject, UserEntity user) {
		var entity = new UserAnthropometersEntity();
		entity.setId(UUID.randomUUID().toString());
		entity.setKatchMcArdle(apiObject.getKatchMcArdle());
		entity.setAge(apiObject.getAge());
		entity.setActivity(apiObject.getActivity());
		entity.setWeight(apiObject.getWeight());
		entity.setHeight(apiObject.getHeight());
		entity.setWaist(apiObject.getWaist());
		entity.setForearm(apiObject.getForearm());
		entity.setWrist(apiObject.getWrist());
		entity.setHip(apiObject.getHip());
		entity.setBmr(apiObject.getBmr());
		entity.setBfp(apiObject.getBfp());
		entity.setBfw(apiObject.getBfw());
		entity.setLbmFemale(apiObject.getLbmFemale());
		entity.setLbmMale(apiObject.getLbmMale());
		entity.setBmi(apiObject.getBmi());
		entity.setUser(user);
		return entity;
	}
	
		
	public GenderEntity genderDtoToEntity(Gender gender) {
		var entity = new GenderEntity();
		entity.setId(gender.getId());
		entity.setGenderType(gender.getGenderType());
		return entity;
	}
	
	public UserEntity userDtoToEntity(Date regstration, GenderEntity gender, String userKeyckloakId) {
		var entity = new UserEntity();
		entity.setId(userKeyckloakId);
		entity.setGender(gender);
		entity.setRegistration(regstration);
		return entity;
	}
	
	public DietCalendarEntity dailyDietDtoToCalendarEntity(DailyDietEntity dto, LocalDate date, UserEntity user) {
		DietCalendarEntity entity = new DietCalendarEntity();
		entity.setId(UUID.randomUUID().toString());
		entity.setConsumption(date);
		entity.setDailyDietId(dto.getId());
		entity.setDietPlanningId(dto.getDietPlanning().getId());
		entity.setUser(user);
		entity.setBreakfastId(dto.getBreakfastId());
		entity.setBreakfastName(dto.getBreakfastName());
		entity.setBrunchId(dto.getBrunchId());
		entity.setBrunchName(dto.getBrunchName());
		entity.setLunchSoupId(dto.getLunchSoupId());
		entity.setLunchSoupName(dto.getLunchSoupName());
		entity.setLunchMainDishId(dto.getLunchMainDishId());
		entity.setLunchMainDishName(dto.getLunchMainDishName());
		entity.setSnackId(dto.getSnackId());
		entity.setSnackName(dto.getSnackName());
		entity.setDinnerId(dto.getDinnerId());
		entity.setDinnerName(dto.getDinnerName());
		return entity;
	}
}
