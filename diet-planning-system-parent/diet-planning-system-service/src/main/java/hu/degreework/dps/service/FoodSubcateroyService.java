package hu.degreework.dps.service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import hu.degreework.dps.api.FoodSubcategory;
import hu.degreework.dps.persistence.FoodSubcategoryEntity;
import hu.degreework.dps.persistence.FoodSubcategoryRepository;
import hu.degreework.dps.service.objmanager.ObjectMapper;
import hu.degreework.dps.service.objmanager.ProcessDtoToEntity;

@Service
public class FoodSubcateroyService {

	private final FoodSubcategoryRepository repository;
	private final ObjectMapper mapper;
	private final ProcessDtoToEntity processDtoToEntity;
	
	public FoodSubcateroyService(FoodSubcategoryRepository repository, ObjectMapper mapper, ProcessDtoToEntity processDtoToEntity) {
		this.repository = repository;
		this.mapper = mapper;
		this.processDtoToEntity = processDtoToEntity;
	}
	
	public Set<FoodSubcategory> getAll(){
		return repository.findAll().stream().map(mapper::foodSubcategoryMap).collect(Collectors.toSet());
	}
	
	public FoodSubcategory create(FoodSubcategory foodSubcategory){
		var response = repository.save(processDtoToEntity.foodSubCatprocessDtoToEntity(foodSubcategory));
		return mapper.foodSubcategoryMap(response);
	}
	
	public List<FoodSubcategory> createAll(List<FoodSubcategory> foodSubcategores){
		List<FoodSubcategoryEntity> response =  foodSubcategores.stream()
				.map(foodSubcategory -> repository.save(processDtoToEntity.foodSubCatprocessDtoToEntity(foodSubcategory)))
				.collect(Collectors.toList());
		return response.stream().map( entity -> mapper.foodSubcategoryMap(entity)).collect(Collectors.toList());
	}

	public FoodSubcategory findById(String id) {
		return mapper.foodSubcategoryMap(repository.findById(id).get());
	}

	public void deleteById(String id) {
		repository.deleteById(id);
	}

}
