package hu.degreework.dps.service;

import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import hu.degreework.dps.api.FoodMaterial;
import hu.degreework.dps.persistence.FoodMaterialRepository;
import hu.degreework.dps.service.exception.FoodMaterialNotFound;
import hu.degreework.dps.service.objmanager.ObjectMapper;

@Service
public class FoodMaterialService {

	private final FoodMaterialRepository repository;
	private final ObjectMapper mapper;

	public FoodMaterialService(FoodMaterialRepository repository, ObjectMapper mapper) {
		this.repository = repository;
		this.mapper = mapper;
	}

	public Set<FoodMaterial> getAll() {
		return repository.findAll().stream().map(mapper::foodMaterialMap).collect(Collectors.toSet());
	}

	public FoodMaterial getById(String id) throws FoodMaterialNotFound {
		var result = repository.findById(id);
		if (!result.isPresent()) {
			throw new FoodMaterialNotFound();
		}
		return mapper.foodMaterialMap(result.get());
	}

}
