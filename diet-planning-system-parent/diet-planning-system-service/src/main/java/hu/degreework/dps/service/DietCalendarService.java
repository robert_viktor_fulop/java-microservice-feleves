package hu.degreework.dps.service;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import hu.degreework.dps.api.DietCalendar;
import hu.degreework.dps.persistence.DailyDietEntity;
import hu.degreework.dps.persistence.DailyDietRepository;
import hu.degreework.dps.persistence.DietCalendarEntity;
import hu.degreework.dps.persistence.DietCalendarRepository;
import hu.degreework.dps.persistence.UserEntity;
import hu.degreework.dps.persistence.UserRepository;
import hu.degreework.dps.service.objmanager.ObjectMapper;
import hu.degreework.dps.service.objmanager.ProcessDtoToEntity;

@Service
public class DietCalendarService {

	private final DietCalendarRepository dietCalendarRepository;
	private final ProcessDtoToEntity processDtoToEntity;
	private final ObjectMapper objectMapper;
	private final UserRepository userRepository;
	private final DailyDietRepository dailyRepsotory;

	public DietCalendarService(UserRepository userRepository, ProcessDtoToEntity processDtoToEntity,
			DietCalendarRepository dietCalendarRepository, DailyDietRepository dailyRepsotory,
			ObjectMapper objectMapper) {

		this.dietCalendarRepository = dietCalendarRepository;
		this.processDtoToEntity = processDtoToEntity;
		this.userRepository = userRepository;
		this.dailyRepsotory = dailyRepsotory;
		this.objectMapper = objectMapper;
	}

	public void save(List<DietCalendar> dietCalendar, String userId) {
		UserEntity user = userRepository.findById(userId).get();
		List<DailyDietEntity> entites;
		DietCalendarEntity dietCalendarEntity;

		LocalDate tomorrow;
		for (DietCalendar diet : dietCalendar) {
			entites = dailyRepsotory.findAll().stream()
					.filter(element -> element.getDietPlanning().getId().equals(diet.getPlannedDietId()))
					.collect(Collectors.toList());

			for (int dayDiet = 0; dayDiet < entites.size(); dayDiet++) {
				dietCalendarEntity = processDtoToEntity.dailyDietDtoToCalendarEntity(entites.get(dayDiet),
						diet.getStartDate(), user);
				dietCalendarRepository.save(dietCalendarEntity);
				tomorrow = diet.getStartDate().plusDays(1);
				diet.setStartDate(tomorrow);
			}

		}
	}

	public List<DietCalendar> getAll(String userId) {
		LocalDate now = LocalDate.now();

		List<DietCalendar> result = dietCalendarRepository.findAll().stream()
				.filter(element -> element.getUser().getId().equals(userId)
						&& element.getConsumption().isAfter(now.minusDays(31)))
				.map(objectMapper::dietCalendarMap)
				.collect(Collectors.toList());
		result.sort(Comparator.comparing(DietCalendar::getStartDate));
		
		return result;
	}
}
