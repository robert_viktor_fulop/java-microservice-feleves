package hu.degreework.dps.service;

import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import hu.degreework.dps.api.Gender;
import hu.degreework.dps.persistence.GenderRepository;
import hu.degreework.dps.service.exception.GenderNotFound;
import hu.degreework.dps.service.objmanager.ObjectMapper;

@Service
public class GenderService {

	private final GenderRepository repository;
	private final ObjectMapper mapper;
	
	public GenderService(GenderRepository repository, ObjectMapper mapper) {
		this.repository = repository;
		this.mapper = mapper;
	}
	
	public Set<Gender> getAll(){
		return repository.findAll().stream().map(mapper::genderMap).collect(Collectors.toSet());
	}
	
	public Gender getById(String id) throws GenderNotFound {
		var result = repository.findById(id);
		if (!result.isPresent()) {
			throw new GenderNotFound();
		}
		return mapper.genderMap(result.get());
	}
}
