package hu.degreework.dps.service;

import java.util.ArrayList;

import org.decimal4j.util.DoubleRounder;
import org.springframework.stereotype.Service;

import hu.degreework.dps.api.Anthropometers;
import hu.degreework.dps.api.BodyMassIndex;
import hu.degreework.dps.api.BodyFatPercentage;
import hu.degreework.dps.api.EnergyDemand;
import hu.degreework.dps.service.validators.AnthropometersCheking;

@Service
public class CalculatorsService {

	// One Kilogram How Many Pounds
	private final double pound = 2.2046226218488;
	// One Pounds How Many Kilogram
	private final double kg = 0.45359237;
	// One centimeter how many inches
	private final double inch = 0.39370078740157;
	// One inch how many centimeter
	private final double cm = 2.54;

	private Double bfwPound;
	private Double lbmPound;
	private Double weightPound;
	private Double waistInch;
	private Double forearmInch;
	private Double wristInch;
	private Double hipInch;

	public BodyMassIndex bmiCalculation(Anthropometers anthropometers, BodyMassIndex bmi) {
		if (!AnthropometersCheking.anthropometersHaveThisValue(new ArrayList<Double>() {
			{
				add(anthropometers.getWeight());
				add(anthropometers.getHeight());
			}
		})) {
			double meter = anthropometers.getHeight() / 100;
			bmi.setBmi(DoubleRounder.round((anthropometers.getWeight() / Math.pow(meter, 2)), 1));
		}
		return bmi;
	}

	public EnergyDemand energyDemandCalculationFemale(Anthropometers anthropometers, EnergyDemand ed) {
		if (!checkData(anthropometers)) {
			ed.setBmrMin(DoubleRounder.round(655.1 + (9.563 * anthropometers.getWeight())
					+ (1.85 * anthropometers.getHeight()) - (4.676 * anthropometers.getAge()), 2));
			ed.setTotalDailyEenergyConsumption(
					DoubleRounder.round(katchMcArdleMultiplier(anthropometers, ed.getBmrMin()), 2));
		}
		return ed;
	}

	public EnergyDemand energyDemandCalculationMale(Anthropometers anthropometers, EnergyDemand ed) {
		if (!checkData(anthropometers)) {
			ed.setBmrMin(DoubleRounder.round(66.47 + (13.75 * anthropometers.getWeight())
					+ (5.003 * anthropometers.getHeight()) - (6.755 * anthropometers.getAge()), 2));
			ed.setTotalDailyEenergyConsumption(
					DoubleRounder.round(katchMcArdleMultiplier(anthropometers, ed.getBmrMin()), 2));
		}
		return ed;
	}

	private Double katchMcArdleMultiplier(Anthropometers anthropometers, Double bmr) {
		Double totalEnergyDemand = bmr;

		switch (anthropometers.getKatchMcArdle()) {
		case SEDENTARINESS:
			totalEnergyDemand = bmr * 1.2;
			break;
		case MODERATE_EXERCISE:
			totalEnergyDemand = bmr * 1.375;
			break;
		case ACTIVE_EXERCISE:
			totalEnergyDemand = bmr * 1.55;
			break;
		case VERY_ACTIVE_EXERCISE:
			totalEnergyDemand = bmr * 1.725;
			break;
		}
		return totalEnergyDemand;
	}

	private boolean checkData(Anthropometers anthropometers) {
		return AnthropometersCheking.anthropometersHaveThisValue(new ArrayList<Double>() {
			{
				add(anthropometers.getWeight());
				add(anthropometers.getHeight());
				add((double) anthropometers.getAge());
			}
		});
	}

	public BodyFatPercentage bodyFatPercentageFemale(Anthropometers anthropometers,
			BodyFatPercentage bodyFatPercentag) {
		if (!AnthropometersCheking.anthropometersHaveThisValue(new ArrayList<Double>() {
			{
				add(anthropometers.getWeight());
				add(anthropometers.getWaist());
				add(anthropometers.getForearm());
				add(anthropometers.getWrist());
				add(anthropometers.getHip());
			}
		})) {
			weightPound = anthropometers.getWeight() * pound;
			waistInch = anthropometers.getWaist() * inch;
			forearmInch = anthropometers.getForearm() * inch;
			wristInch = anthropometers.getWrist() * inch;
			hipInch = anthropometers.getHip() * inch;

			lbmPound = leanBodyMassFemale();
			bodyFatPercentag.setLbmFemale(DoubleRounder.round((lbmPound * kg), 3));
			bodyFatPercentag = bodyFatPercentage(bodyFatPercentag);
		}
		return bodyFatPercentag;
	}

	public BodyFatPercentage bodyFatPercentageMale(Anthropometers anthropometers, BodyFatPercentage bodyFatPercentag) {
		if (!AnthropometersCheking.anthropometersHaveThisValue(new ArrayList<Double>() {
			{
				add(anthropometers.getWeight());
				add(anthropometers.getWaist());
			}
		})) {
			weightPound = anthropometers.getWeight() * pound;
			waistInch = anthropometers.getWaist() * inch;
			lbmPound = leanBodyMassMale();
			bodyFatPercentag.setLbmMale(DoubleRounder.round((lbmPound * kg), 3));
			bodyFatPercentag = bodyFatPercentage(bodyFatPercentag);
		}
		return bodyFatPercentag;
	}

	public BodyFatPercentage bodyFatPercentage(BodyFatPercentage bodyFatPercentag) {

		bfwPound = bodyFatWeight();
		bodyFatPercentag.setBfw(DoubleRounder.round((bfwPound * kg), 3));
		bodyFatPercentag.setBfp(DoubleRounder.round(((bfwPound / weightPound) * 100), 3));
		return bodyFatPercentag;
	}

	public Double bodyFatWeight() {
		return weightPound - lbmPound;
	}

	public Double leanBodyMassFemale() {
		return (weightPound * 0.732) + 8.987 + (wristInch / 3.140) - (waistInch * 0.157) - (hipInch * 0.249)
				+ (forearmInch * 0.434);
	}

	public Double leanBodyMassMale() {
		return (weightPound * 1.082) + 94.42 - (waistInch * 4.15);
	}

}
