package hu.degreework.dps.service.validators;

import java.util.List;
import java.util.stream.Collectors;

public class AnthropometersCheking {

	public static Boolean anthropometersHaveThisValue(List<Double> data) {
		return data.stream().map( d -> (d != null && d > 0)).collect(Collectors.toList()).contains(false);
	}
	
	public static Boolean anthropometerResultCheck(List<Double> resultValues, Double lbmMale, Double lbmFemale) {
		Boolean checking = true;
		List<Double> check = resultValues.stream().filter(res -> res != null).collect(Collectors.toList());
		
		if(check.size() != resultValues.size() || (lbmMale == null && lbmFemale  == null)) {
			checking = false;
		}
		
		return checking;
	}
}
