package hu.degreework.dps.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import hu.degreework.dps.api.DietGenerate;
import hu.degreework.dps.api.GeneratedDiet;
import hu.degreework.dps.api.GeneratedDiets;
import hu.degreework.dps.api.LunchGeneratedDiet;
import hu.degreework.dps.api.MealPreferenceType;
import hu.degreework.dps.api.PartOfDayType;
import hu.degreework.dps.api.PriceCategoryType;
import hu.degreework.dps.persistence.DailyDietEntity;
import hu.degreework.dps.persistence.DailyDietRepository;
import hu.degreework.dps.persistence.DietPlanningEntity;
import hu.degreework.dps.persistence.DietPlanningRepository;
import hu.degreework.dps.persistence.FoodMaterialRepository;
import hu.degreework.dps.persistence.IngredientEntity;
import hu.degreework.dps.persistence.IngredientRepository;
import hu.degreework.dps.persistence.OilPreferencesRepository;
import hu.degreework.dps.persistence.RecipeEntity;
import hu.degreework.dps.persistence.RecipeRepository;
import hu.degreework.dps.persistence.SugarPreferencesRepository;
import hu.degreework.dps.persistence.UserAnthropometersEntity;
import hu.degreework.dps.persistence.UserAnthropometersRepository;
import hu.degreework.dps.persistence.UserEntity;
import hu.degreework.dps.persistence.UserRepository;
import hu.degreework.dps.service.exception.DietGenerateNotFound;
import hu.degreework.dps.service.exception.UserAnthropometersNotFound;
import hu.degreework.dps.service.objmanager.ObjectMapper;

@Service
public class DietGenerateService {
	private final RecipeRepository recipeRepository;
	private final UserRepository userRepository;
	private final UserAnthropometersRepository anthRepository;
	private final IngredientRepository ingredientRepository;
	private final FoodMaterialRepository foodRepository;
	private final OilPreferencesRepository oilRepository;
	private final SugarPreferencesRepository sugarRepository;
	private final DailyDietRepository dailyRepsotory;
	private final DietPlanningRepository dietPlRepository;
	private final ObjectMapper mapper;
	
	List<RecipeEntity> breakfast = new ArrayList<RecipeEntity>();
	List<RecipeEntity> brunch = new ArrayList<RecipeEntity>();
	List<RecipeEntity> lunchSoup = new ArrayList<RecipeEntity>();
	List<RecipeEntity> lunchMainDish = new ArrayList<RecipeEntity>();
	List<RecipeEntity> snack = new ArrayList<RecipeEntity>();
	List<RecipeEntity> dinner = new ArrayList<RecipeEntity>();
	Map<String, RecipeEntity> recipesReady;
	Map<String, RecipeEntity> recipesAfternoonReady;

	List<Map<String, RecipeEntity>> recipesReadyList = new ArrayList<Map<String, RecipeEntity>>();
	List<PartOfDayType> lookingForPartOfDay = new ArrayList<>();
	PartOfDayType skipPartOfDayType = null;

	Double morningMax;
	Double afternoonMax;
	Random rand = new Random();
	int sizeBreakfast;
	int sizeBrunch;
	int sizeSoup;
	int sizeMainDish;
	int sizeSnack;
	int sizeDinner;
	Double dailyCalorieIntake;
	int maxCalcNumber = 0;
	int min = 1000;

	public DietGenerateService(RecipeRepository recipeRepository, UserRepository userRepository,
			UserAnthropometersRepository anthRepository, IngredientRepository ingredientRepository,
			FoodMaterialRepository foodRepository, OilPreferencesRepository oilRepository,
			SugarPreferencesRepository sugarRepository, DietPlanningRepository dietPlRepository,
			DailyDietRepository dailyRepsotory, ObjectMapper mapper) {
		this.recipeRepository = recipeRepository;
		this.userRepository = userRepository;
		this.anthRepository = anthRepository;
		this.ingredientRepository = ingredientRepository;
		this.foodRepository = foodRepository;
		this.oilRepository = oilRepository;
		this.sugarRepository = sugarRepository;
		this.dietPlRepository = dietPlRepository;
		this.dailyRepsotory = dailyRepsotory;
		this.mapper = mapper;
	}

	public List<GeneratedDiets> dietGeneration(DietGenerate dietGenerate, String userId) throws DietGenerateNotFound {
		Map<String, String> needs = new HashMap<String, String>() {
			{
				put("OilPreferencesType", dietGenerate.getDietOil().toString());
				put("MealPreferenceType", dietGenerate.getDietMealType().toString());
				put("PriceCategoryType", dietGenerate.getDietPrice().toString());
				put("SugarPreferencesType", dietGenerate.getDietSugar().toString());
			}
		};

		for (String key : needs.keySet()) {
			if (needs.get(key).equals("NONE") || needs.get(key).equals("NO_SELECTION") || needs.get(key) == null) {
				needs.put(key, "NO_SELECTION");
			}
		}

		List<RecipeEntity> result = new ArrayList<RecipeEntity>();
		result = this.recipeRepository.findAll().stream().collect(Collectors.toList());

		if (!needs.get("OilPreferencesType").equals("NO_SELECTION")) {
			result = result.stream()
					.filter(x -> x.getOilPreferences().getName().toString().equals("NO_SELECTION")
							|| x.getOilPreferences().getName().toString().equals(needs.get("OilPreferencesType")))
					.collect(Collectors.toList());
		}

		if (!needs.get("MealPreferenceType").equals("NO_SELECTION")) {
			skipPartOfDayType = dietGenerate.getDietMealType().equals(MealPreferenceType.NO_BRUNCH) ? PartOfDayType.BRUNCH
					: PartOfDayType.SNACK;
			result = result.stream()
					.filter(x -> !x.getPartOfDay().getName().toString().equals(skipPartOfDayType.toString()))
					.collect(Collectors.toList());
		}

		if (!needs.get("PriceCategoryType").equals("NO_SELECTION")) {
			result = result.stream()
					.filter(x -> x.getPriceCategory().getName().toString().equals("NO_SELECTION")
							|| x.getPriceCategory().getName().toString().equals(needs.get("PriceCategoryType")))
					.collect(Collectors.toList());
		}

		if (!needs.get("SugarPreferencesType").equals("NO_SELECTION")) {
			result = result.stream()
					.filter(x -> x.getSugarPreferences().getName().toString().equals("NO_SELECTION")
							|| x.getSugarPreferences().getName().toString().equals(needs.get("SugarPreferencesType")))
					.collect(Collectors.toList());
		}

		List<UserAnthropometersEntity> userAnthropometers = anthRepository.findAll().stream()
				.filter(useranth -> useranth.getUser().getId().equals(userId)).collect(Collectors.toList());

		Double energy = null;

		if (!userAnthropometers.isEmpty()) {
			energy = userAnthropometers.stream().max(Comparator.comparing(UserAnthropometersEntity::getTimestamp))
					.map(userAnthr -> userAnthr.getBmr()).get();
		}

		morningMax = energy * 0.7;
		afternoonMax = energy * 0.3;

		breakfast = result.stream().filter(recipe -> recipe.getPartOfDay().getName() == PartOfDayType.BREAKFAST)
				.collect(Collectors.toList());
		brunch = result.stream().filter(recipe -> recipe.getPartOfDay().getName() == PartOfDayType.BRUNCH)
				.collect(Collectors.toList());
		lunchSoup = result.stream()
				.filter(recipe -> recipe.getPartOfDay().getName() == PartOfDayType.LUNCH && recipe.getSoup() == true)
				.collect(Collectors.toList());
		lunchMainDish = result.stream()
				.filter(recipe -> recipe.getPartOfDay().getName() == PartOfDayType.LUNCH && recipe.getSoup() == false)
				.collect(Collectors.toList());
		snack = result.stream().filter(recipe -> recipe.getPartOfDay().getName().equals(PartOfDayType.SNACK))
				.collect(Collectors.toList());
		dinner = result.stream().filter(recipe -> recipe.getPartOfDay().getName().equals(PartOfDayType.DINNER))
				.collect(Collectors.toList());

		if (breakfast.isEmpty() || lunchMainDish.isEmpty() || lunchSoup.isEmpty() || dinner.isEmpty()) {
			throw new DietGenerateNotFound("Nincs elegendő recept, kérem válasszon más preferenciát!");
		}

		sizeBreakfast = breakfast.size();
		sizeBrunch = brunch.size();
		sizeSoup = lunchSoup.size();
		sizeMainDish = lunchMainDish.size();
		sizeSnack = snack.size();
		sizeDinner = dinner.size();

		List<GeneratedDiets> generatedDietsList = new ArrayList<GeneratedDiets>();
		GeneratedDiets generatedDiets;
		GeneratedDiet dto;
		LunchGeneratedDiet lunches;

		int indexBreakFast;
		int indexBrunch;
		int indexSoup;
		int indexMainDish;
		int indexSnack;
		int indexDinner;

		for (int i = 0; i < dietGenerate.getDietPeriod(); i++) {
			maxCalcNumber = 0;
			recipesReady = new HashMap<String, RecipeEntity>();

			indexBreakFast = rand.nextInt(sizeBreakfast);
			indexBrunch = skipPartOfDayType != PartOfDayType.BRUNCH ? rand.nextInt(sizeBrunch) : 0;
			indexSoup = rand.nextInt(sizeSoup);
			indexMainDish = rand.nextInt(sizeMainDish);

			morningCalculation(indexBreakFast, indexBrunch, indexSoup, indexMainDish, false);

			generatedDiets = new GeneratedDiets();
			dto = new GeneratedDiet();
			dto.setId(recipesReady.get(PartOfDayType.BREAKFAST.toString()).getId());
			dto.setName(recipesReady.get(PartOfDayType.BREAKFAST.toString()).getName());
			dto.setCalorie(recipesReady.get(PartOfDayType.BREAKFAST.toString()).getEnergy());
			generatedDiets.setBreakfast(dto);

			if (skipPartOfDayType != PartOfDayType.BRUNCH) {
				dto = new GeneratedDiet();
				dto.setId(recipesReady.get(PartOfDayType.BRUNCH.toString()).getId());
				dto.setName(recipesReady.get(PartOfDayType.BRUNCH.toString()).getName());
				dto.setCalorie(recipesReady.get(PartOfDayType.BRUNCH.toString()).getEnergy());
				generatedDiets.setBrunch(dto);
			}

			lunches = new LunchGeneratedDiet();
			dto = new GeneratedDiet();
			dto.setId(recipesReady.get(PartOfDayType.LUNCH.toString() + "_SOUP").getId());
			dto.setName(recipesReady.get(PartOfDayType.LUNCH.toString() + "_SOUP").getName());
			dto.setCalorie(recipesReady.get(PartOfDayType.LUNCH.toString() + "_SOUP").getEnergy());
			lunches.setSoup(dto);

			dto = new GeneratedDiet();
			dto.setId(recipesReady.get(PartOfDayType.LUNCH.toString() + "_MAIN_DISH").getId());
			dto.setName(recipesReady.get(PartOfDayType.LUNCH.toString() + "_MAIN_DISH").getName());
			dto.setCalorie(recipesReady.get(PartOfDayType.LUNCH.toString() + "_MAIN_DISH").getEnergy());
			lunches.setMainDish(dto);
			generatedDiets.setLunch(lunches);

			generatedDiets.setDailyCalorieIntake(dailyCalorieIntake);
			generatedDietsList.add(generatedDiets);
		}

		for (int i = 0; i < dietGenerate.getDietPeriod(); i++) {
			maxCalcNumber = 0;

			recipesAfternoonReady = new HashMap<String, RecipeEntity>();

			indexSnack = skipPartOfDayType != PartOfDayType.SNACK ? rand.nextInt(sizeSnack) : 0;
			indexDinner = rand.nextInt(sizeDinner);

			afternoonCalculation(indexSnack, indexDinner, false);
			generatedDietsList.get(i).setDailyCalorieIntake(
					Double.sum(generatedDietsList.get(i).getDailyCalorieIntake(), dailyCalorieIntake));

			if (skipPartOfDayType != PartOfDayType.SNACK) {
				dto = new GeneratedDiet();
				dto.setId(recipesAfternoonReady.get(PartOfDayType.SNACK.toString()).getId());
				dto.setName(recipesAfternoonReady.get(PartOfDayType.SNACK.toString()).getName());
				dto.setCalorie(recipesAfternoonReady.get(PartOfDayType.SNACK.toString()).getEnergy());
				generatedDietsList.get(i).setSnack(dto);
			}

			dto = new GeneratedDiet();
			dto.setId(recipesAfternoonReady.get(PartOfDayType.DINNER.toString()).getId());
			dto.setName(recipesAfternoonReady.get(PartOfDayType.DINNER.toString()).getName());
			dto.setCalorie(recipesAfternoonReady.get(PartOfDayType.DINNER.toString()).getEnergy());
			generatedDietsList.get(i).setDinner(dto);

		}
		return generatedDietsList;
	}

	public void morningCalculation(int indexBreakFast, int indexBrunch, int indexLunchSoup, int indexLunchMainDish,
			boolean end) {
		if (maxCalcNumber <= 50 && !end) {
			maxCalcNumber = maxCalcNumber + 1;

			recipesReady.put(PartOfDayType.BREAKFAST.toString(), breakfast.get(indexBreakFast));
			dailyCalorieIntake = recipesReady.get(PartOfDayType.BREAKFAST.toString()).getEnergy().doubleValue();

			if (dailyCalorieIntake <= morningMax && skipPartOfDayType != PartOfDayType.BRUNCH) {
				recipesReady.put(PartOfDayType.BRUNCH.toString(), brunch.get(indexBrunch));
				dailyCalorieIntake = Double.sum(dailyCalorieIntake,
						recipesReady.get(PartOfDayType.BRUNCH.toString()).getEnergy().doubleValue());
				indexBrunch = rand.nextInt(sizeBrunch);
			}

			if (dailyCalorieIntake <= morningMax) {
				recipesReady.put(PartOfDayType.LUNCH.toString() + "_SOUP", lunchSoup.get(indexLunchSoup));
				dailyCalorieIntake = Double.sum(dailyCalorieIntake,
						recipesReady.get(PartOfDayType.LUNCH.toString() + "_SOUP").getEnergy().doubleValue());
			}

			if (dailyCalorieIntake <= morningMax) {
				recipesReady.put(PartOfDayType.LUNCH.toString() + "_MAIN_DISH", lunchMainDish.get(indexLunchMainDish));
				dailyCalorieIntake = Double.sum(dailyCalorieIntake,
						recipesReady.get(PartOfDayType.LUNCH.toString() + "_MAIN_DISH").getEnergy().doubleValue());
			}
			boolean allPartOfDay = false;

			if (skipPartOfDayType != PartOfDayType.BRUNCH) {
				allPartOfDay = recipesReady.keySet().size() != 3;
			} else {
				allPartOfDay = recipesReady.keySet().size() != 4;
			}

			indexBreakFast = rand.nextInt(sizeBreakfast);
			indexLunchSoup = rand.nextInt(sizeSoup);
			indexLunchMainDish = rand.nextInt(sizeMainDish);

			if (!allPartOfDay) {
				morningCalculation(indexBreakFast, indexBrunch, indexLunchSoup, indexLunchMainDish, false);
			}

			if (dailyCalorieIntake < morningMax && dailyCalorieIntake >= min) {
				morningCalculation(indexBreakFast, indexBrunch, indexLunchSoup, indexLunchMainDish, true);
			}

			if (dailyCalorieIntake > morningMax || dailyCalorieIntake < min) {
				morningCalculation(indexBreakFast, indexBrunch, indexLunchSoup, indexLunchMainDish, false);
			}

		} else {

			if (recipesReady.isEmpty()) {
				recipesReady.put(PartOfDayType.BREAKFAST.toString(), breakfast.get(indexBreakFast));
				dailyCalorieIntake = recipesReady.get(PartOfDayType.BREAKFAST.toString()).getEnergy().doubleValue();

				if (dailyCalorieIntake <= morningMax && skipPartOfDayType != PartOfDayType.BRUNCH) {
					recipesReady.put(PartOfDayType.BRUNCH.toString(), brunch.get(indexBrunch));
					dailyCalorieIntake = Double.sum(dailyCalorieIntake,
							recipesReady.get(PartOfDayType.BRUNCH.toString()).getEnergy().doubleValue());
				}

				recipesReady.put(PartOfDayType.LUNCH.toString() + "_SOUP", lunchSoup.get(indexLunchSoup));
				dailyCalorieIntake = Double.sum(dailyCalorieIntake,
						recipesReady.get(PartOfDayType.LUNCH.toString() + "_SOUP").getEnergy().doubleValue());

				recipesReady.put(PartOfDayType.LUNCH.toString() + "_MAIN_DISH", lunchMainDish.get(indexLunchMainDish));
				dailyCalorieIntake = Double.sum(dailyCalorieIntake,
						recipesReady.get(PartOfDayType.LUNCH.toString() + "_MAIN_DISH").getEnergy().doubleValue());
			}
		}

	}

	public void afternoonCalculation(int indexSnack, int indexDinner, boolean end) {
		if (maxCalcNumber <= 50 && !end) {
			maxCalcNumber = maxCalcNumber + 1;
			dailyCalorieIntake = 0.0;

			if (dailyCalorieIntake <= afternoonMax && skipPartOfDayType != PartOfDayType.SNACK) {
				recipesAfternoonReady.put(PartOfDayType.SNACK.toString(), snack.get(indexSnack));
				dailyCalorieIntake = Double.sum(dailyCalorieIntake,
						recipesAfternoonReady.get(PartOfDayType.SNACK.toString()).getEnergy().doubleValue());
				indexSnack = rand.nextInt(sizeSnack);
			}

			if (dailyCalorieIntake <= afternoonMax) {
				recipesAfternoonReady.put(PartOfDayType.DINNER.toString(), dinner.get(indexDinner));
				dailyCalorieIntake = Double.sum(dailyCalorieIntake,
						recipesAfternoonReady.get(PartOfDayType.DINNER.toString()).getEnergy().doubleValue());
			}
			boolean allPartOfDay = false;

			if (skipPartOfDayType != PartOfDayType.SNACK) {
				allPartOfDay = recipesAfternoonReady.keySet().size() != 1;
			} else {
				allPartOfDay = recipesAfternoonReady.keySet().size() != 2;
			}

			indexDinner = rand.nextInt(sizeDinner);

			if (!allPartOfDay) {
				afternoonCalculation(indexSnack, indexDinner, false);
			}

			if (dailyCalorieIntake < afternoonMax && dailyCalorieIntake >= min) {
				afternoonCalculation(indexSnack, indexDinner, true);
			}

			if (dailyCalorieIntake > afternoonMax || dailyCalorieIntake < min) {
				afternoonCalculation(indexSnack, indexDinner, false);
			}

		} else {

			if (recipesAfternoonReady.isEmpty()) {

				if (skipPartOfDayType != PartOfDayType.SNACK) {
					recipesAfternoonReady.put(PartOfDayType.SNACK.toString(), snack.get(indexSnack));
					dailyCalorieIntake = Double.sum(dailyCalorieIntake,
							recipesAfternoonReady.get(PartOfDayType.SNACK.toString()).getEnergy().doubleValue());
					indexSnack = rand.nextInt(sizeSnack);
				}

				recipesAfternoonReady.put(PartOfDayType.DINNER.toString(), dinner.get(indexDinner));
				dailyCalorieIntake = Double.sum(dailyCalorieIntake,
						recipesAfternoonReady.get(PartOfDayType.DINNER.toString()).getEnergy().doubleValue());
			}
		}
	}

	public void save(List<GeneratedDiets> generatedDiets, String userId) {
		DietPlanningEntity entityPlanning = new DietPlanningEntity();
		entityPlanning.setId(UUID.randomUUID().toString());
		entityPlanning.setUser(userRepository.findById(userId).get());
		DietPlanningEntity entityPlanningResult = dietPlRepository.save(entityPlanning);
		String uuidDietPlanning = entityPlanningResult.getId();
		String[] dailyDiets = new String[generatedDiets.size()];
		int index = 0;
		String uuid;
		
		for (GeneratedDiets generatedDiets2 : generatedDiets) {
			DailyDietEntity entityDaily = new DailyDietEntity();
			uuid = UUID.randomUUID().toString();
			dailyDiets[index] = uuid;
			index = index + 1;
			entityDaily.setId(uuid);
			
			entityDaily.setBreakfastId(generatedDiets2.getBreakfast().getId());	
			entityDaily.setBreakfastCalorie(generatedDiets2.getBreakfast().getCalorie());
			entityDaily.setBreakfastName(generatedDiets2.getBreakfast().getName());
			
			
			if (generatedDiets2.getBrunch() != null) {		
				entityDaily.setBrunchId(generatedDiets2.getBrunch().getId());	
				entityDaily.setBrunchCalorie(generatedDiets2.getBrunch().getCalorie());
				entityDaily.setBrunchName(generatedDiets2.getBrunch().getName());
			}
			
			entityDaily.setLunchSoupId(generatedDiets2.getLunch().getSoup().getId());	
			entityDaily.setLunchSoupCalorie(generatedDiets2.getLunch().getSoup().getCalorie());
			entityDaily.setLunchSoupName(generatedDiets2.getLunch().getSoup().getName());
			
			entityDaily.setLunchMainDishId(generatedDiets2.getLunch().getMainDish().getId());	
			entityDaily.setLunchMainDishCalorie(generatedDiets2.getLunch().getMainDish().getCalorie());
			entityDaily.setLunchMainDishName(generatedDiets2.getLunch().getMainDish().getName());
			
			if (generatedDiets2.getSnack() != null) {				
				entityDaily.setSnackId(generatedDiets2.getSnack().getId());	
				entityDaily.setSnackCalorie(generatedDiets2.getSnack().getCalorie());
				entityDaily.setSnackName(generatedDiets2.getSnack().getName());
			}
						
			entityDaily.setDinnerId(generatedDiets2.getDinner().getId());	
			entityDaily.setDinnerCalorie(generatedDiets2.getDinner().getCalorie());
			entityDaily.setDinnerName(generatedDiets2.getDinner().getName());
			
			entityDaily.setDailyCalorieIntake(generatedDiets2.getDailyCalorieIntake());
			entityDaily.setDietPlanning(entityPlanningResult);
			entityDaily.setNumber(index);
			dailyRepsotory.save(entityDaily);
		}
		
	}
}
