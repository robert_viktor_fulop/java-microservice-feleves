package hu.degreework.dps.service.objmanager;


import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Component;

import hu.degreework.dps.api.DietCalendar;
import hu.degreework.dps.api.FoodCategory;
import hu.degreework.dps.api.FoodMaterial;
import hu.degreework.dps.api.FoodSubcategory;
import hu.degreework.dps.api.Gender;
import hu.degreework.dps.api.Ingredient;
import hu.degreework.dps.api.PlannedDiet;
import hu.degreework.dps.api.Recipe;
import hu.degreework.dps.api.UserAnthropometers;
import hu.degreework.dps.persistence.DietCalendarEntity;
import hu.degreework.dps.persistence.DietPlanningEntity;
import hu.degreework.dps.persistence.FoodCategroyEntity;
import hu.degreework.dps.persistence.FoodSubcategoryEntity;
import hu.degreework.dps.persistence.GenderEntity;
import hu.degreework.dps.persistence.IngredientEntity;
import hu.degreework.dps.persistence.RecipeEntity;
import hu.degreework.dps.persistence.UserAnthropometersEntity;
import hu.degreework.dps.persistence.FoodMaterialEntity;

@Component
public class ObjectMapper {

	public FoodSubcategory foodSubcategoryMap(FoodSubcategoryEntity entity) {
		var dto = new FoodSubcategory();
		dto.setId(entity.getId());
		dto.setName(entity.getSubcategory());

		return dto;
	}

	public FoodCategory foodCategoryMap(FoodCategroyEntity entity) {
		var dto = new FoodCategory();
		dto.setId(entity.getId());
		dto.setName(entity.getCategory());

		return dto;
	}

	public UserAnthropometers userAnthropometersMap(UserAnthropometersEntity entity) {
		var dto = new UserAnthropometers();
		dto.setId(entity.getId());
		dto.setKatchMcArdle(entity.getKatchMcArdle());
		dto.setAge(entity.getAge());
		dto.setActivity(entity.getActivity());
		dto.setWeight(entity.getWeight());
		dto.setHeight(entity.getHeight());
		dto.setWaist(entity.getWaist());
		dto.setForearm(entity.getForearm());
		dto.setWrist(entity.getWrist());
		dto.setHip(entity.getHip());
		dto.setBmr(entity.getBmr());
		dto.setBfp(entity.getBfp());
		dto.setBfw(entity.getBfw());
		dto.setLbmFemale(entity.getLbmFemale());
		dto.setLbmMale(entity.getLbmMale());
		dto.setBmi(entity.getBmi());
		dto.setTimestamp(entity.getTimestamp());
		dto.setGender(entity.getUser().getGender().getGenderType());
		dto.setBirthday(entity.getUser().getBirthday());
		return dto;
	}

	public FoodMaterial foodMaterialMap(FoodMaterialEntity entity) {
		var dto = new FoodMaterial();
		dto.setId(entity.getId());
		dto.setName(entity.getName());
		dto.setCategory(entity.getCategory().getCategory());
		dto.setSubcategory(entity.getSubcategory().getSubcategory());
		dto.setEnergyKcal(entity.getEnergyKcal());
		dto.setProteinGram(entity.getProteinGram());
		dto.setFatGram(entity.getFatGram());
		dto.setCarbohydrateGram(entity.getCarbohydrateGram());
		dto.setGi(entity.getGi());

		return dto;
	}

	public Gender genderMap(GenderEntity entity) {
		var dto = new Gender();
		dto.setId(entity.getId());
		dto.setGenderType(entity.getGenderType());
		return dto;
	}
	

	public Set<Recipe> recipeMap(Map<RecipeEntity, List<IngredientEntity>> ingredientsMap) {
		Set<Recipe> recipes = new HashSet<Recipe>();
		Set<Ingredient> ingredients = null;
		for (RecipeEntity recipeEntity : ingredientsMap.keySet()) {
			var dto = new Recipe();
			dto.setId(recipeEntity.getId());
			dto.setName(recipeEntity.getName());
			dto.setaVitamin(recipeEntity.getaVitamin());
			dto.setbOneVitamin(recipeEntity.getbOneVitamin());
			dto.setbTwoVitamin(recipeEntity.getbTwoVitamin());
			dto.setCalcium(recipeEntity.getCalcium());
			dto.setcVitamin(recipeEntity.getcVitamin());
			dto.setdVitamin(recipeEntity.getdVitamin());
			dto.setkVitamin(recipeEntity.getkVitamin());
			dto.setIron(recipeEntity.getIron());
			dto.setCarbohydrate(recipeEntity.getCarbohydrate());
			dto.setEnergy(recipeEntity.getEnergy());
			dto.setMagneseium(recipeEntity.getMagneseium());
			dto.setRecipePreparation(recipeEntity.getRecipePreparation());
			dto.setFat(recipeEntity.getFat());
			dto.setProtein(recipeEntity.getProtein());
			dto.setPartOfDay(recipeEntity.getPartOfDay().getName());
			dto.setPriceCategory(recipeEntity.getPriceCategory().getName());
			dto.setOilPreferences(recipeEntity.getOilPreferences().getName());
			dto.setSugarPreferences(recipeEntity.getSugarPreferences().getName());
			ingredients = new HashSet<Ingredient>();
			for (IngredientEntity ingredientEntity : ingredientsMap.get(recipeEntity)) {
				var ingredient = new Ingredient();
				ingredient.setId(ingredientEntity.getId());
				ingredient.setQuantity(ingredientEntity.getQuantity());
				ingredient.setFoodMaterialName(ingredientEntity.getFoodMaterials().getName());
				ingredient.setUnitOfMeasures(ingredientEntity.getUnitOfMeasures().getName());				
				ingredient.setRecipeId(ingredientEntity.getRecipes().getId());				
				ingredients.add(ingredient);
			}
			dto.setIngredients(ingredients);
			recipes.add(dto);
		}

		return recipes;
	}
	
	public PlannedDiet dietPlanningMap(DietPlanningEntity entity) {
		var dto = new PlannedDiet();
		//dto.setId(entity.getId());
		//dto.setDailyDiets(entity.getDailyDites().stream().map( key -> key.getId()).collect(Collectors.toSet()));
		return dto;
	}
	
	public DietCalendar dietCalendarMap(DietCalendarEntity entity) {
		DietCalendar dto = new DietCalendar();
		dto.setId(entity.getId());
		dto.setStartDate(entity.getConsumption());
		dto.setDailyDietId(entity.getDailyDietId());
		dto.setPlannedDietId(entity.getDietPlanningId());
		dto.setBreakfastId(entity.getBreakfastId());
		dto.setBreakfastName(entity.getBreakfastName());
		dto.setBrunchId(entity.getBrunchId());
		dto.setBrunchName(entity.getBrunchName());
		dto.setLunchSoupId(entity.getLunchSoupId());
		dto.setLunchSoupName(entity.getLunchSoupName());
		dto.setLunchMainDishId(entity.getLunchMainDishId());
		dto.setLunchMainDishName(entity.getLunchMainDishName());
		dto.setSnackId(entity.getSnackId());
		dto.setSnackName(entity.getSnackName());
		dto.setDinnerId(entity.getDinnerId());
		dto.setDinnerName(entity.getDinnerName());
		return dto;
	}
}
