package hu.degreework.dps.service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import hu.degreework.dps.api.FoodCategory;
import hu.degreework.dps.persistence.FoodCategoryRepository;
import hu.degreework.dps.persistence.FoodCategroyEntity;
import hu.degreework.dps.service.objmanager.ObjectMapper;
import hu.degreework.dps.service.objmanager.ProcessDtoToEntity;

@Service
public class FoodCategoryService {
	private final FoodCategoryRepository repository;
	private final ObjectMapper mapper;
	private final ProcessDtoToEntity processDtoToEntity;
	
	public FoodCategoryService(FoodCategoryRepository repository, ObjectMapper mapper,  ProcessDtoToEntity processDtoToEntity) {
		this.repository = repository;
		this.mapper = mapper;
		this.processDtoToEntity = processDtoToEntity;
	}
	
	public Set<FoodCategory> getAll(){
		return repository.findAll().stream().map(mapper::foodCategoryMap).collect(Collectors.toSet());
	}
	
	public FoodCategory create(FoodCategory foodCategory){
		var response = repository.save(processDtoToEntity.foodCatprocessDtoToEntity(foodCategory));
		return mapper.foodCategoryMap(response);
	}
	
	public List<FoodCategory> createAll(List<FoodCategory> foodCategores){
		List<FoodCategroyEntity> response =  foodCategores.stream()
				.map(foodCategory -> repository.save(processDtoToEntity.foodCatprocessDtoToEntity(foodCategory)))
				.collect(Collectors.toList());
		return response.stream().map( entity -> mapper.foodCategoryMap(entity)).collect(Collectors.toList());
	}

	public FoodCategory findById(String id) {
		return mapper.foodCategoryMap(repository.findById(id).get());
	}

	public void deleteById(String id) {
		repository.deleteById(id);
	}
	
}
