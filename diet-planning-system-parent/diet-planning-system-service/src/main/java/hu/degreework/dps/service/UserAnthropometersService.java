package hu.degreework.dps.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import hu.degreework.dps.api.Anthropometers;
import hu.degreework.dps.api.BodyFatPercentage;
import hu.degreework.dps.api.BodyMassIndex;
import hu.degreework.dps.api.EnergyDemand;
import hu.degreework.dps.api.UserAnthropometers;
import hu.degreework.dps.persistence.GenderEntity;
import hu.degreework.dps.persistence.GenderRepository;
import hu.degreework.dps.persistence.UserAnthropometersEntity;
import hu.degreework.dps.persistence.UserAnthropometersRepository;
import hu.degreework.dps.persistence.UserEntity;
import hu.degreework.dps.persistence.UserRepository;
import hu.degreework.dps.service.exception.UserAnthropometersNotFound;
import hu.degreework.dps.service.objmanager.ObjectMapper;
import hu.degreework.dps.service.objmanager.ProcessDtoToEntity;
import hu.degreework.dps.service.validators.AnthropometersCheking;

@Service
public class UserAnthropometersService {
	private final UserRepository userReposiotry;
	private final GenderRepository genderRepository;
	private final CalculatorsService service;
	private final UserAnthropometersRepository anthRepository;
	private final ObjectMapper mapper;
	private final ProcessDtoToEntity processDtoToEntity;
	private final Anthropometers anthropometers = new Anthropometers();
	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	private final Date date = new Date(System.currentTimeMillis());

	public UserAnthropometersService(CalculatorsService service, UserAnthropometersRepository repository,
			ObjectMapper mapper, ProcessDtoToEntity processDtoToEntity, UserRepository userReposiotry,
			GenderRepository genderRepository) {
		this.anthRepository = repository;
		this.mapper = mapper;
		this.processDtoToEntity = processDtoToEntity;
		this.service = service;
		this.userReposiotry = userReposiotry;
		this.genderRepository = genderRepository;
	}

	public UserAnthropometers create(UserAnthropometers userAnth, String userKeycloakId)
			throws UserAnthropometersNotFound {
		var user = userReposiotry.findById(userKeycloakId);

		if (!user.isPresent()) {
			throw new UserAnthropometersNotFound("Az adatbázisban még nem létezik a felhasználó!");
		}

		UserEntity userEntity = user.get();
		if (!userEntity.getGenderAndBirthdayCompleted()) {
			if (userAnth.getBirthday() == null || userAnth.getGender() == null) {
				throw new UserAnthropometersNotFound(
						"A felhasználó születésnapja és neme adatainak kitöltése kötelező!");

			} else {
				GenderEntity gender = genderRepository.findAll().stream()
						.filter(name -> name.getGenderType().equals(userAnth.getGender())).map(element -> element)
						.collect(Collectors.collectingAndThen(Collectors.toList(), list -> {
							if (list.size() != 1) {
								throw new UserAnthropometersNotFound("Nem létező 'gender' elem!");
							}
							return list.get(0);
						}));
				UserEntity updateEntity = new UserEntity();
				updateEntity.setId(userKeycloakId);
				updateEntity.setGender(gender);
				updateEntity.setBirthday(userAnth.getBirthday());
				updateEntity.setGenderAndBirthdayCompleted(true);
				userEntity = userReposiotry.save(updateEntity);
			}
		}

		Set<UserAnthropometersEntity> resultUserAnthr = anthRepository.findAll().stream()
				.filter(anthropometers -> anthropometers.getUser().getId().equals(userKeycloakId))
				.collect(Collectors.toSet());

		if (!resultUserAnthr.isEmpty()) {
			UserAnthropometersEntity resultUserAnthrSimple = resultUserAnthr.stream()
					.max(Comparator.comparing(UserAnthropometersEntity::getTimestamp)).get();
			try {

				Date createAnthropometer = sdf.parse(sdf.format(resultUserAnthrSimple.getTimestamp()));
				Date todaysDate = sdf.parse(sdf.format(date));
				if (createAnthropometer.equals(todaysDate)) {
					throw new UserAnthropometersNotFound("A felhasználó egy napon csak egy feltöltést végezhet!");
				}
				
			} catch (ParseException e) {
				throw new UserAnthropometersNotFound("Dátum konverzió közben hiba történt: " + e.getMessage());
			}
		}

		Period period = LocalDate.parse(sdf.format(userEntity.getBirthday()))
				.until(LocalDate.parse(sdf.format(date)));
		userAnth.setAge(period.getYears());
		anthropometers.setKatchMcArdle(userAnth.getKatchMcArdle());
		anthropometers.setGender(userAnth.getGender());
		anthropometers.setAge(userAnth.getAge());
		anthropometers.setActivity(userAnth.getActivity());
		anthropometers.setWeight(userAnth.getWeight());
		anthropometers.setHeight(userAnth.getHeight());
		anthropometers.setWaist(userAnth.getWaist());
		anthropometers.setForearm(userAnth.getForearm());
		anthropometers.setWrist(userAnth.getWrist());
		anthropometers.setHip(userAnth.getHip());
		userAnth.setBmi(bmiCalculation());
		userAnth.setBmr(energyDemandCalculation());
		BodyFatPercentage bodyFatPercentage = bodyFatPercentageCalculation();
		userAnth.setBfw(bodyFatPercentage.getBfw());
		userAnth.setBfp(bodyFatPercentage.getBfp());
		userAnth.setLbmFemale(bodyFatPercentage.getLbmFemale());
		userAnth.setLbmMale(bodyFatPercentage.getLbmMale());

		Boolean checking = AnthropometersCheking.anthropometerResultCheck(new ArrayList<>() {
			{
				add(userAnth.getBmi());
				add(userAnth.getBmr());
				add(userAnth.getBfw());
				add(userAnth.getBfp());
			}
		}, userAnth.getLbmFemale(), userAnth.getLbmMale());

		if (checking) {
			var response = anthRepository.save(processDtoToEntity.userAnthropometersDtoToEntity(userAnth, userEntity));
			return mapper.userAnthropometersMap(response);
		}

		return userAnth;
	}

	public UserAnthropometers findByUserId(String userKeycloakId) throws UserAnthropometersNotFound {

		var resultUser = userReposiotry.findById(userKeycloakId);

		if (!resultUser.isPresent()) {
			UserEntity userEntity = new UserEntity();
			userEntity.setId(userKeycloakId);
			userEntity.setGenderAndBirthdayCompleted(false);
			userReposiotry.save(userEntity);
		}

		List<UserAnthropometersEntity> result = anthRepository.findAll().stream()
				.filter(useranth -> useranth.getUser().getId().equals(userKeycloakId)).collect(Collectors.toList());

		UserAnthropometers userAnthropometers = new UserAnthropometers();

		if (!result.isEmpty()) {
			userAnthropometers = mapper.userAnthropometersMap(
					result.stream().max(Comparator.comparing(UserAnthropometersEntity::getTimestamp)).get());
		}

		return userAnthropometers;
	}

	public Double bmiCalculation() {
		return service.bmiCalculation(anthropometers, new BodyMassIndex()).getBmi();
	}

	public Double energyDemandCalculation() {
		Double result = null;

		switch (anthropometers.getGender()) {
		case FEMALE:
			result = service.energyDemandCalculationFemale(anthropometers, new EnergyDemand())
					.getTotalDailyEenergyConsumption();

		case MALE:
			result = service.energyDemandCalculationMale(anthropometers, new EnergyDemand())
					.getTotalDailyEenergyConsumption();

		default:
			return result;
		}
	}

	public BodyFatPercentage bodyFatPercentageCalculation() {
		BodyFatPercentage bodyFatPercentag = new BodyFatPercentage();

		switch (anthropometers.getGender()) {
		case FEMALE:
			return service.bodyFatPercentageFemale(anthropometers, bodyFatPercentag);

		case MALE:
			return service.bodyFatPercentageMale(anthropometers, bodyFatPercentag);

		default:
			return bodyFatPercentag;
		}
	}
}
