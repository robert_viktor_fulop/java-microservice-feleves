package hu.degreework.dps.service;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import hu.degreework.dps.api.Recipe;
import hu.degreework.dps.persistence.IngredientEntity;
import hu.degreework.dps.persistence.IngredientRepository;
import hu.degreework.dps.persistence.RecipeEntity;
import hu.degreework.dps.persistence.RecipeRepository;
import hu.degreework.dps.service.objmanager.ObjectMapper;

@Service
public class RecipeService {

	private final RecipeRepository recipeRepository;
	private final IngredientRepository ingredientRepository;
	private final ObjectMapper mapper;

	private RecipeService(RecipeRepository recipeRepository, ObjectMapper mapper,
			IngredientRepository ingredientRepository) {
		this.recipeRepository = recipeRepository;
		this.ingredientRepository = ingredientRepository;
		this.mapper = mapper;
	}

	public Set<Recipe> getAll(Integer pageNo, Integer pageSize) {
		Pageable pageable = PageRequest.of(pageNo, pageSize);
		List<String> reci = recipeRepository.findAll(pageable).stream().map(key -> key.getId())
				.collect(Collectors.toList());
		List<IngredientEntity> ingredientsEntity = ingredientRepository.findAll().stream()
				.filter(recipeKey -> reci.contains(recipeKey.getRecipes().getId())).collect(Collectors.toList());
		Map<RecipeEntity, List<IngredientEntity>> ingredientsMap = ingredientsEntity.stream()
				.collect(Collectors.groupingBy(x -> x.getRecipes()));
		return mapper.recipeMap(ingredientsMap);
	}

	public Set<Recipe> getAll() {
		Map<RecipeEntity, List<IngredientEntity>> ingredientsMap = ingredientRepository.findAll().stream()
				.collect(Collectors.groupingBy(x -> x.getRecipes()));
		return mapper.recipeMap(ingredientsMap);
	}
}
