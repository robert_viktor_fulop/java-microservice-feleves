package hu.degreework.dps.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import hu.degreework.dps.api.GeneratedDiet;
import hu.degreework.dps.api.GeneratedDiets;
import hu.degreework.dps.api.LunchGeneratedDiet;
import hu.degreework.dps.api.PlannedDiet;
import hu.degreework.dps.persistence.DailyDietEntity;
import hu.degreework.dps.persistence.DailyDietRepository;
import hu.degreework.dps.persistence.DietCalendarRepository;
import hu.degreework.dps.persistence.DietPlanningRepository;
import hu.degreework.dps.persistence.RecipeRepository;
import hu.degreework.dps.persistence.UserRepository;
import hu.degreework.dps.service.objmanager.ObjectMapper;
import hu.degreework.dps.service.objmanager.ProcessDtoToEntity;

@Service
public class PlannedDietService {

	private final DailyDietRepository dailyRepsotory;
	private final DietPlanningRepository dietPlRepository;

	public PlannedDietService(DailyDietRepository dailyRepsotory, DietPlanningRepository dietPlRepository,
			ObjectMapper mapper, ProcessDtoToEntity processDtoToEntity, RecipeRepository recipeRepository,
			UserRepository userRepository, DietCalendarRepository dietCalendarRepository) {
		this.dailyRepsotory = dailyRepsotory;
		this.dietPlRepository = dietPlRepository;
	}

	public List<PlannedDiet> getAll(String userId) {
		Set<DailyDietEntity> dailyDiets = dailyRepsotory.findAll().stream()
				.filter(id -> id.getDietPlanning().getUser().getId().equals(userId)).collect(Collectors.toSet());
		List<String> dietPlanningIds = dietPlRepository.findAll().stream()
				.filter(id -> id.getUser().getId().equals(userId)).map(entity -> entity.getId())
				.collect(Collectors.toList());

		List<GeneratedDiets> generatedDietsListDto;
		GeneratedDiets generatedDietsDto;
		GeneratedDiet breakfastDto;
		GeneratedDiet brunchDto;
		GeneratedDiet snackDto;
		GeneratedDiet dinnerDto;
		GeneratedDiet soupDto;
		GeneratedDiet mainDishDto;
		LunchGeneratedDiet lunchGeneratedDietDto;
		PlannedDiet plannedDiet;
		List<PlannedDiet> plannedDiets = new ArrayList<PlannedDiet>();

		for (String dietPlanningId : dietPlanningIds) {
			List<DailyDietEntity> dailyDietsOnePeriod = dailyDiets.stream()
					.filter(entity -> entity.getDietPlanning().getId().equals(dietPlanningId))
					.collect(Collectors.toList());
			plannedDiet = new PlannedDiet();
			generatedDietsListDto = new ArrayList<GeneratedDiets>();

			for (DailyDietEntity entity : dailyDietsOnePeriod) {
				generatedDietsDto = new GeneratedDiets();
				breakfastDto = new GeneratedDiet();
				brunchDto = new GeneratedDiet();
				snackDto = new GeneratedDiet();
				dinnerDto = new GeneratedDiet();
				soupDto = new GeneratedDiet();
				mainDishDto = new GeneratedDiet();
				lunchGeneratedDietDto = new LunchGeneratedDiet();

				soupDto.setId(entity.getLunchSoupId());
				soupDto.setName(entity.getLunchSoupName());
				soupDto.setCalorie(entity.getLunchSoupCalorie());

				mainDishDto.setId(entity.getLunchMainDishId());
				mainDishDto.setName(entity.getLunchMainDishName());
				mainDishDto.setCalorie(entity.getLunchMainDishCalorie());

				lunchGeneratedDietDto.setSoup(soupDto);
				lunchGeneratedDietDto.setMainDish(mainDishDto);

				breakfastDto.setId(entity.getBreakfastId());
				breakfastDto.setName(entity.getBreakfastName());
				breakfastDto.setCalorie(entity.getBreakfastCalorie());

				brunchDto.setId(entity.getBrunchId());
				brunchDto.setName(entity.getBrunchName());
				brunchDto.setCalorie(entity.getBrunchCalorie());

				snackDto.setId(entity.getSnackId());
				snackDto.setName(entity.getSnackName());
				snackDto.setCalorie(entity.getSnackCalorie());

				dinnerDto.setId(entity.getDinnerId());
				dinnerDto.setName(entity.getDinnerName());
				dinnerDto.setCalorie(entity.getDinnerCalorie());

				generatedDietsDto.setBreakfast(breakfastDto);
				generatedDietsDto.setBrunch(brunchDto);
				generatedDietsDto.setLunch(lunchGeneratedDietDto);
				generatedDietsDto.setSnack(snackDto);
				generatedDietsDto.setDinner(dinnerDto);

				generatedDietsDto.setDailyCalorieIntake(entity.getDailyCalorieIntake());
				generatedDietsDto.setNumber(entity.getNumber());
				generatedDietsListDto.add(generatedDietsDto);
			}
			generatedDietsListDto.sort(Comparator.comparing(GeneratedDiets::getNumber));
			plannedDiet.setId(dietPlanningId);
			plannedDiet.setDiets(generatedDietsListDto);
			plannedDiets.add(plannedDiet);
		}
		return plannedDiets;
	}

	public void delete(String plannedDietId, String userId) {
		List<DailyDietEntity> dailyIds = dailyRepsotory.findAll().stream()
				.filter(element -> element.getDietPlanning().getId().equals(plannedDietId))
				.collect(Collectors.toList());

		for (DailyDietEntity element : dailyIds) {
			dailyRepsotory.delete(element);
		}

		dietPlRepository.deleteById(plannedDietId);
	}


}
