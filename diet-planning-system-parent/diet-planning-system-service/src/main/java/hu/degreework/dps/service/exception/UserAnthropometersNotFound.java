package hu.degreework.dps.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UserAnthropometersNotFound extends RuntimeException {

	public UserAnthropometersNotFound(String message) {
		super(message);
	}
}
