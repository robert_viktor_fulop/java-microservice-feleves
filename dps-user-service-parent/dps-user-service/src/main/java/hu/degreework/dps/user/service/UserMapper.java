package hu.degreework.dps.user.service;

import org.springframework.stereotype.Component;

import hu.degreework.dps.user.api.UserMeta;
import hu.degreework.dps.user.keycloak.api.KeycloakUser;

@Component
public class UserMapper {

    public UserMeta map(KeycloakUser keycloakUser) {
        var meta = new UserMeta();
        meta.setId(keycloakUser.getId());
        meta.setEmail(keycloakUser.getEmail());
        meta.setFirstName(keycloakUser.getFirstName());
        meta.setLastName(keycloakUser.getLastName());
        meta.setUserName(keycloakUser.getUserName());
        return meta;
    }

}
