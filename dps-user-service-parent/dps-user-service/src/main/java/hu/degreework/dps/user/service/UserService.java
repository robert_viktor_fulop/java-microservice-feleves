package hu.degreework.dps.user.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import hu.degreework.dps.user.api.UserMeta;
import hu.degreework.dps.user.keycloak.api.KeycloakApi;

@Service
public class UserService {

    private final KeycloakApi api;
    private final UserMapper mapper;
    private final List<String> userKeyList = new ArrayList<String>() {{
    	add("email");
    	add("family_name");
    	add("name");
    	add("preferred_username");
    }};
    public UserService(KeycloakApi api, UserMapper mapper) {
        this.api = api;
        this.mapper = mapper;
    }

    public UserMeta getUser(String realm, String id, Map<String, Object> userData) throws UserNotFound {
        if(!id.isBlank()) {
        	Map<String, String> resultUserData = userDataMapper(userData);
            var user = new UserMeta();
            user.setId(id);
            user.setEmail(resultUserData.get("email"));
            user.setFirstName(resultUserData.get("family_name"));
            user.setLastName( resultUserData.get("name") != null ? resultUserData.get("name").toString().split(" ")[0] : resultUserData.get("name"));
            user.setUserName(resultUserData.get("preferred_username"));
            return user;
        } else {
            throw new UserNotFound();
        }
    }
    
    public Map<String, String> userDataMapper(Map<String, Object> userData){
    	Map<String, String> resultUserData = new HashMap<String, String>();
    	for (String userKey : userKeyList) {
			if(userData.get(userKey) != null) {
				resultUserData.put(userKey, userData.get(userKey).toString());
			} else {
				resultUserData.put(userKey, null);
			}
		}
    	return resultUserData;	
    }

}
