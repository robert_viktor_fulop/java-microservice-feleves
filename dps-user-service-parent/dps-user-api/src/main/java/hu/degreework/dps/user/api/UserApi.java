package hu.degreework.dps.user.api;


import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/api/users")
public interface UserApi {
    
    @GetMapping(value = "/current", produces = MediaType.APPLICATION_JSON_VALUE)
    UserMeta getUser();

}
