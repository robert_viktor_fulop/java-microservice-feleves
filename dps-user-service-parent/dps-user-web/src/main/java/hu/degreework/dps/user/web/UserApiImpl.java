package hu.degreework.dps.user.web;


import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import hu.degreework.dps.user.api.UserApi;
import hu.degreework.dps.user.api.UserMeta;
import hu.degreework.dps.user.service.UserNotFound;
import hu.degreework.dps.user.service.UserService;

@RestController
public class UserApiImpl implements UserApi {

	private final UserService service;
	private final KeycloakTokenHelper helper;

	public UserApiImpl(UserService service, KeycloakTokenHelper helper) {
		this.service = service;
		this.helper = helper;
	}

	@Override
	public UserMeta getUser() {
		try {
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			var jwtToken = (Jwt) authentication.getPrincipal();
			Map<String, Object> userData = jwtToken.getClaims();
			return service.getUser(helper.getRealmFromJwt(jwtToken), helper.getIdFromJwt(jwtToken), userData);
		} catch (UserNotFound e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}
	

}
