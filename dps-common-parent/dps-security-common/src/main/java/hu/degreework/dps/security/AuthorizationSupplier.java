package hu.degreework.dps.security;

import java.util.function.Supplier;

@FunctionalInterface
public interface AuthorizationSupplier extends Supplier<String> {

}
